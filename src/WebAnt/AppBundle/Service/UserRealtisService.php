<?php
/**
 * Created by PhpStorm.
 * User: kwant
 * Date: 18.05.15
 * Time: 14:37
 */

namespace WebAnt\AppBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Entity\User;
use WebAnt\AppBundle\Entity\UserInfo;
use WebAnt\AppBundle\Data\Data;


Class UserRealtisService extends Controller{


    private $em;

    public function __construct(){
//        $this->em = $this->getContainer()->get('doctrine')->getManager();
//        $em = new EntityManager();
//        $em = $this->getDoctrine()->getManager();
    }

    public function checkUser($em,$u){
        $uf = $u->getUserInfo();
        $ufid = $uf->getId();
        if(is_null($ufid)){
            return false;
        }


        $min_price = $uf->getMinPrice()-0;
        $max_price = $uf->getMaxPrice()-0;
        $districts = $uf->getDistricts();

        $qb = $em->createQueryBuilder();
        $qb->select('p')
            ->from('WebAntAppBundle:Realty', 'p');

        $qb->andWhere(
            $qb->expr()->eq('p.status', 4) //только те, что в работе
        );
        $query = $qb;

        $districts = $uf->getDistricts();
        $d = new Data();
        if(count($districts)){
            $query->andWhere(
                $query->expr()->in($query->getRootAliases()[0] . '.distr', ':distr')
            );
            $query->setParameter('distr', $d->ids2strings($districts));
        }
        $types = $uf->getRealtyTypes();
        if(count($types)){
            $query->andWhere(
                $query->expr()->in($query->getRootAliases()[0] . '.type', ':types')
            );
            $query->setParameter('types', $types);
        }
        $min_price = $uf->getMinPrice();
        if($min_price > 0){
            $query->andWhere(
                $query->expr()->gte($query->getRootAliases()[0] . '.price', ':min_price')
            );
            $query->setParameter('min_price', $min_price);
        }
        $max_price = $uf->getMaxPrice();
        if($max_price > 0){
            $query->andWhere(
                $query->expr()->lte($query->getRootAliases()[0] . '.price', ':max_price')
            );
            $query->setParameter('max_price', $max_price);
        }




        $query = $qb->getQuery();
        $objects = $query->getResult();



//        $query = $em->createQuery(
//            'SELECT p
//                FROM WebAntAppBundle:Realty p
//                WHERE p.status != :arc'
//        )->setParameter(':arc', 5);
//        $objects = $query->getResult();

//        if(count($districts)){
//            $query->andWhere(
//                $query->expr()->in($query->getRootAliases()[0] . '.distr', ':distr')
//            );
//            $query->setParameter('distr', $d->ids2strings($districts));
//        }

        return array(
            'count'=> count($objects),
            'objects' => $objects,
            'user'    => $u,
        );
    }

    public function checkAll($em){
        $query = $em->createQuery(
            'SELECT u
                FROM WebAntAppBundle:User u
                WHERE u.roles != :clients'
        )->setParameter(':clients', 'a:1:{i:0;s:16:"ROLE_REALTY_USER";}');
        $users = $query->getResult();
        foreach($users as $u){
            $res[] = $this->checkUser($em,$u);
        }
        return $res;
    }









}




