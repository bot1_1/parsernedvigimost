<?php

namespace WebAnt\AppBundle\Service;

use WebAnt\AppBundle\Entity\History;

class SaveHistory{

    public function saveEntry($em, $who, $type, $params){
        $hist = new History();
        $hist->setUserId($who);
        $hist->setType($type);
        $hist->setParams($params);
        $em->persist($hist);
        $em->flush();
    }





}

