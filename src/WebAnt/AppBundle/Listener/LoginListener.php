<?php

namespace WebAnt\AppBundle\Listener;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use WebAnt\AppBundle\Entity\History;
use Doctrine\ORM\EntityManager;


class LoginListener
{

   public function __construct(SecurityContextInterface $security, EntityManager $em)
   {
      $this->security = $security;
      $this->em = $em;
   }

   public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
   {
       $user = $this->security->getToken()->getUser();

       $hist = new History();
       $hist->setUserId($user);
       $hist->setType('login');
       $hist->setParams('');
       $this->em->persist($hist);
       $this->em->flush();


//        $timezone = $this->security->getToken()->getUser()->getTimezone();
//        if (empty($timezone)) {
//            $timezone = 'UTC';
//        }
//        $this->session->set('timezone', $timezone);
   }

}