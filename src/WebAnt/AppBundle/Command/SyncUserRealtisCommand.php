<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Entity\User;
use WebAnt\AppBundle\Entity\UserInfo;
use WebAnt\AppBundle\Data\Data;

use WebAnt\AppBundle\Service\UserRealtisService;
use WebAnt\AppBundle\Service\SmsService;


class SyncUserRealtisCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('sync:user:realty')
            ->setDescription('send sms to users')
            ->addArgument('arg1',null,'1st argument(not used yet)')
        ;
    }

    private $em;


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $svc = new UserRealtisService();
        $sms = new SmsService();
        $this->em = $this->getContainer()->get('doctrine')->getManager();

//        $svc->getAllRealtis($this->em);

        $res = $svc->checkAll($this->em);

        $u = new User();

        $dummy = false;
        $arg1 = $input->getArgument('arg1');
        if($arg1 == 'dummy'){
            $dummy = true;
        }


        foreach($res as $r){
            if( $r['count'] &&
                $r['user']->getRole0() == 'ROLE_REALTY_USER' &&
                !$r['user']->isLocked() &&
                $r['user']->isEnabled()
            ){
//                var_dump($r['count']);

//                var_dump($r['user']->getUserInfo()->getPhone());
                $ph = $r['user']->getUserInfo()->getPhone();
                $count = $r['count'];
                $ph = preg_replace("/[^0-9]/","",$ph);
                $id  = $r['user']->getId();
                $fio = $r['user']->getFIO();


                if($count == 0){
                    print_r('клиенту №$id $fio не подходит ни один объект\n');
                }
                if(!$ph){
                    print_r('у клиента №$id $fio нет телефона\n');
                }

                if($ph && $count){
//                    $rt = "объявлений";
//                    if($count%10==1&&$count%100!=11)
//                        $rt = "объявление";
//                    if($count%10>=2&&$count%10<=4&&($count%100<=10||$count%100>=20))
//                        $rt = "объявления";
//
//                    print_r("sendSms $ph \"на ваш запрос найдено $count $rt\"\n");
//                    $sms ->  sendSms($ph, "на ваш запрос найдено $count $rt");
                    foreach($r['objects'] as $realty){
                        $str = array('Комната','1к-кв','2к-кв','3к-кв','','Дом')[$realty->getType()];

                        if($realty->getPrice()>0){
                            $str .= ', '.$realty->getPrice().'р';
                        }
                        if($realty->getAddr()){
                            $addr = $realty->getAddr();
                            $addr = preg_replace('/г\. Ростов-на-дону,/','', $addr);
                            $str .= ', '.$addr;
                        }
                        if($realty->getPhone()){
                            $str .= ', '.$realty->getPhone();
                        }
                        print_r("sendSms $ph \"$str\"\n");
                        if(!$dummy){
                            $sms ->  sendSms($ph, $str);
                        }
                    }
                }
            }
        }




//                    print_r(
//                        "объект №".$r->getId().
//                        ", находящийся по адресу  ".$r->getDistr().", ".$r->getAddr().
//                        " с арендной платой ". $r->getPrice().
//                        "\n".
//
//                        "подходит пользователю ".$u->getFIO().
//                        "\n\n"
//        print_r(microtime(true)-$begin);
//        print_r("\n");

    }
}
