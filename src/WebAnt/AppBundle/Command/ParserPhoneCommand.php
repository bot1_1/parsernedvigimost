<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Command\NeagentParser;
use TesseractOCR;
use WebAnt\AppBundle\Command\RuCaptcha;

class ParserPhoneCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('parser:parse:phone')
            ->setDescription('parse Phones')
            ->addArgument('arg1',null,'1st argument(not used yet)')
        ;
    }

    private $ch;
    private $em;

    private function getApiKey(){
        $np = new NeagentParser();
        return $np->getApiKey();
    }

    private function request($ch, $id, $h, $captcha){

        $fields = array(
            'id'   => $id,
            'h'    => $h,
            'captcha'=> $captcha,
            'rand' =>  rand(100500,999999)
        );

//        $fields = array(
//            'id'   => 123456,
//            'h'    => 12345,
//            'captcha'=> '',
//            'rand' => 432507,
//        );


        $Url = 'http://neagent.info/client/phone/img/';
        //Set the URL to work with
        //var_dump($Url);

        curl_setopt($ch, CURLOPT_URL, $Url);

        // ENABLE HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);

        $fields_string = http_build_query($fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept-Language:ru,en;q=0.8'
        ));

/*
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept:',
            'Accept-Encoding:gzip,deflate,sdch',
            'Accept-Language:ru,en;q=0.8',
            'Content-Type:application/x-www-form-urlencoded; charset=UTF-8',
            'Content-Length:66',
            'Host:neagent.info',
            'Origin:http://neagent.info',
            'Referer:http://neagent.info/client/home/',
            'X-Requested-With:XMLHttpRequest'
        ));
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
*/
        //Set the post parameters
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);


        return curl_exec($ch);
    }



    private function getRealty($id){
        $realty = new Realty();
        $this->em->persist($realty);
        $query = $this->em->createQuery(
            'SELECT p
                FROM WebAntAppBundle:Realty p
                WHERE p.id = :id'
        )->setParameter('id', $id);
        $res = $query->getResult();
        return count($res)?$res[0]:null;
    }


    private function getCodes($id){
        $realty = new Realty();
        $this->em->persist($realty);
        $query = $this->em->createQuery(
            'SELECT p
                FROM WebAntAppBundle:Realty p
                WHERE p.id = :id'
        )->setParameter('id', $id);
        $res = $query->getResult();
        $this->em->clear();

        if(count($res)){
            return array(
                'h'     => $res[0]->getPhoneShow(),
                'id'    => $res[0]->getOldId(),
                'phone' => $res[0]->getPhone(),
            );
        } else {
            return false;
        }
    }


    //http://neagent.info/client/phone/img/

    private function getCaptcha($ch){
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $rand = rand(100500,999999);
        curl_setopt($ch, CURLOPT_URL, "http://neagent.info/client/phone/captcha/?r=$rand");

        $filename = 'tmp/captcha.jpg';

        $fp = fopen($filename, 'wb');

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        fclose($fp);

        $realpath = realpath($filename);

        $cap = new RuCaptcha();
        $apikey = $this->getApiKey();
        $result = $cap ->recognize($realpath,$apikey);
        unlink($filename);

        curl_setopt($ch, CURLOPT_FILE, STDOUT);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        return $result;
    }



    private function parsePhone($ch, $id, $captcha = ""){
        $codes = $this->getCodes($id);
        //get the code for getting image;
        $result = $this->request($ch, $codes['id'], $codes['h'], $captcha);
        //get base64 of image
        if(substr($result,0,3) =="cap"){
            $captcha = $this->getCaptcha($ch);
            return $this->parsePhone($ch,$id,$captcha);
        } else {
            return $this->doParsePhone($id, $result);
        }
    }

    private function doParsePhone($id, $base64){

        $file    = "web/phoneimgs/$id.png";
        if (!file_exists('web/phoneimgs/')) {
            mkdir('web/phoneimgs/', 0775, true);
        }
        if (!file_exists('tmp')) {
            mkdir('tmp', 0775, true);
        }
        $ifp = fopen($file, "wb");

        fwrite($ifp, base64_decode($base64));
        fclose($ifp);
        //save image

        if(filesize($file)<50){
            print_r("getting phone for $id fail\n");
            return false;
        }

        $tmpfile = "tmp/tmp_".rand(100500,999999).".png";
        shell_exec("convert $file -resize 300% $tmpfile");

        $ocr = new TesseractOCR($tmpfile);
        $ocr->setWhitelist(range(0,9), '-');
        $phone = $ocr->recognize();
        $phone = preg_replace('/\s+/', '', $phone);
        unlink($tmpfile);

        print_r("(realty.id=$id).phone:=$phone;\n");

        $realty = $this->getRealty($id);
        $this->em->clear();
        $realty->setWho('Парсер(манагер-телефон)');
        $realty->setPhone($phone);
        $this->em->merge($realty);
        $this->em->flush();
        $this->em->clear();
        return true;
    }






//http://neagent.info/client/phone/captcha/?r=154178

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();


        //getting objects for parsing:
        $date = new \DateTime();
        $date->modify('-15 day');

        $query = $this->em->createQuery(
            'SELECT p
                FROM WebAntAppBundle:Realty p
                WHERE p.date > :date
                AND p.phone     is     NULL
                AND p.phoneshow is NOT NULL
                '
        )->setParameter(':date', $date);
        $result = $query->getResult();
        //:objects.get


        //        $arg1 = $input->getArgument('arg1');

        var_dump(count($result));

        $dummy = false;

        $np = new NeagentParser();
        $ch = $np->init();

        if(!$dummy){
            $ch = $np->login($ch);
        }

        $da = true;
        $phones_remain = rand(5,15);
        $iters_remain = 100500;


        while($da){
            $r = $result[array_rand($result)];
            if($r->getPhoneShow() && !$r->getPhone()){
                $id = $r->getId();
                if($dummy){
                    print_r(
                        "need parse phone for id=$id using".
                        " id=".$r->getOldId().
                        " & h=".$r->getPhoneShow().
                        "\n"
                    );
                }

                if(!$dummy){
                    $mt = rand(80E6,120E6);
                    print_r("waiting... $mt ms ");
                    usleep($mt);
                    print_r(
                        "parse phone for id=$id using".
                        " id=".$r->getOldId().
                        " & h=".$r->getPhoneShow());
                    print_r("\n");
                    $this->parsePhone($ch,$id);
                }

                $phones_remain--;
                if($phones_remain<1){
                    $da = false;
                }
            }
            $iters_remain--;
            if($iters_remain<1){
                $da = false;
            }
        }

        if(!$dummy){
            $mt = rand(15E6,25E6);
            usleep($mt);
            $np->logout($ch);
        }

    }
}
