<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use WebAnt\AppBundle\Entity\Realty;


class AnonParserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('parser:parse:anon')
            ->setDescription('parse as anonymous')
            ->addArgument('arg1',null,'1st argument(not used yet)')
        ;
    }

    private $ch;
    private $em;
    private $key = '8p320c11';
    private $proxies = array(
        '127.0.0.1:8118'
    );

    private function getRandProxy(){
        $i = array_rand($this->proxies);
        $proxy = $this->proxies[$i];
        return $proxy;
//        return $this->proxies[array_rand($this->$proxies)];
    }

    private function curl_init()
    {
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);

        //curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));

        //Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
        //not to print out the results of its query.
        //Instead, it will return the results as a string return value
        //from curl_exec() instead of the usual true/false.
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36");
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
    }


    private function login($url){

        //http://neagent.info/rostov/login/

        curl_setopt($this->ch, CURLOPT_POST, 0);
        curl_setopt($this->ch, CURLOPT_URL, "http://neagent.info/rostov/login/");
//id_section=75&url=%2Frostov%2Fbase%2F4985719%2F&key=8p320c11
        $post = http_build_query(array(
            "id_section" => 75,
            "url" => $url,
            "key" => $this->key
        ));

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post);
        return curl_exec($this->ch);
    }





    private function getByOldId($old_id){
        $em = $this->getContainer()->get('doctrine')->getManager();
        $query = $em->createQuery(
            'SELECT p
                FROM WebAntAppBundle:Realty p
                WHERE p.old_id = :old_id'
        )->setParameter('old_id', $old_id);
        $res = $query->getResult();
        return count($res)?$res[0]:null;
    }


    private function request($type, $page){
        $Url = "http://neagent.info/rostov/arenda/$type/";
        if($page>1){
            $Url.="?page=$page";
        }
        curl_setopt($this->ch, CURLOPT_URL, $Url);

        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());

        // ENABLE HTTP POST
//        curl_setopt($this->ch, CURLOPT_POST, 1);

        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Accept-Language:ru,en;q=0.8'
        ));

        return curl_exec($this->ch);
    }


    private function parsePage($type, $page){
        $crawler = new Crawler;
        $crawler->addHTMLContent($page);

        $trs = $crawler->filter('.infoblock4');

        $da = true; // objects are actual


        foreach ($trs as $el) {
            $pq = new Crawler($el);
            $old_id = preg_replace("/[^0-9]/","",$pq->attr('id'));
            $stext = $pq->filter('#atitle')->text();

            $d = $pq->filter('#date')->text();
            $arr = array("Сегодня","Вчера","Позавчера");
            if(in_array($d,$arr)){
                $days = array_search($d,$arr);
                $date = new \DateTime();
                $date->modify("-$days day");
                $info = array(
                    "date"=>$date,
                    "old_id"=>$old_id
                );
                $price = $pq->filter('#price')->text();
                $info["price"] = preg_replace("/[^0-9]/","",$price);
                $objects[] = $info;
            } else {
//                print_r("недвижимость №${info['old_id']} не актуальна, смотрим следующую категорию... \n");
                $da = false;
            }
        }

        $already = 0;

        if(isset($objects)){
            var_dump(count($objects));
            shuffle($objects);
            foreach($objects as $obj){
                $realty = $this->getByOldId($obj["old_id"]);
                if($realty){
                    $date  = new \DateTime();
                    $date  = $date->getTimestamp();
                    $date2 = $realty->getDate()->getTimestamp();
                    if( ($date-$date2)>100500 ){
                        $this->parseObject($type, $obj);
                        usleep(rand(5E6,15E6));
                    } else {
                        $id     = $realty->getId();
                        $old_id = $realty->getOldId();
                        print_r("объект №$old_id($id) уже парсился недавно, пропускаем\n");
                        $already++;
                        usleep(rand(5E5,1E6));
                    };
                } else {
                    $this->parseObject($type, $obj);
                    usleep(rand(5E6,15E6));
                }
            }
        }
        if($already == count($objects)){
            $da = false;
            print_r("мы уже тут видели всё, переходим к следующей категории");
        }
        return $da;
    }

    private function parseObject($type, $info0){
//        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());
        curl_setopt($this->ch, CURLOPT_URL, "http://neagent.info/rostov/base/{$info0["old_id"]}/");
        $html = curl_exec($this->ch);

        $crawler = new Crawler;
        $crawler->addHTMLContent($html);
        $info= array();



        if($crawler->filter('#tdcontent>h1')->count()){
            $info["stext"]   = $crawler->filter('#tdcontent>h1')->text();
        }
        if($crawler->filter('#tdcontent #textcontent')->count()){
            $info["text"] = $crawler->filter('#tdcontent #textcontent')->text();
            $info["text"] = preg_replace('#^\d+#', '', $info["text"]);
            $info["text"] = preg_replace( '/\s*\d+$/', '', $info["text"] );
        }
        if($crawler->filter('#tdcontent #bi_street>span')->count())
        {
            $info["street"]  = $crawler->filter('#tdcontent #bi_street>span')->eq(0)->text();
        }

        if($crawler->filter('#tdcontent #distr')->count())
        {
            $distr   = $crawler->filter('#tdcontent #distr')->html();
            $info["distr"]   = explode('<', $distr)[0];
        }


        $area = "0";
        $a = $crawler->filter('table.baseinfo tr')->eq(2)->text();
        if (strpos($a,'Общая площадь') !== false) {
            $area = $crawler->filter('table.baseinfo tr')->eq(2)->text();
        }
        $a = $crawler->filter('table.baseinfo tr')->eq(3)->text();
        if (strpos($a,'Общая площадь') !== false) {
            $area = $crawler->filter('table.baseinfo tr')->eq(3)->text();
        }
        $area = preg_replace("/[^0-9.,]/","",$area);
        $info["area"] = preg_replace("/,/",".",$area);


        $obj = $this->getByOldId($info0["old_id"]);
        if(!isset($obj)){
            $realty = new Realty();
            $exist = false;
        } else {
            $realty = $obj;
            $exist = true;
        }

        $realty->setType($type);
        $realty->setWho('Парсер(анонимный)');
        $realty->setOldId($info0["old_id"]);
        $realty->setStext($info["stext"]);
        $realty->setText($info["text"]);
        if(array_key_exists("street",$info)){
            $realty->setAddr($info["street"]);
        }
        if(array_key_exists("phone",$info)){
            $realty->setPhone($info["phone"]);
        }
        $realty->setDistr($info["distr"]);
        $realty->setPrice($info0["price"]);
        $realty->setArea($info["area"]);
        $status = $realty->getStatus();
        if($status == 5){
            $realty->setStatus(0);
        }

        $old_id = $realty->getOldId();
        $em = $this->getContainer()->get('doctrine')->getManager();

        if($exist){
            $id = $realty->getId();
            print_r("недвижимость №$old_id уже есть в базе(id=$id), обновляем\n");
            $em->clear();
            $em->merge($realty);
            $em->flush();
            $em->clear();
        }

        if(!$exist){
            print_r("недвижимость №$old_id новая, записываем.\n");
            $em->clear();
            $em->persist($realty);
            $em->flush();
            $em->clear();
        }
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $types = array(
            "sdam-komnatu",
            "sdam-odno-komnatnuyu-kvartiru",
            "sdam-dvuh-komnatnuyu-kvartiru",
            "sdam-treh-komnatnuyu-kvartiru",
            "",
            "sdam-dom",
        );
        $this->proxies = $this->getContainer()->getParameter('proxies');

        $type = 0;
        $this->curl_init();

        foreach(array(0,1,2,3,5)  as $type){
            $page = 1;
            $da = true;
            while($da){
                print_r("parse page $page of type=${types[$type]}\n");
                $ans = $this->request($types[$type],$page);
                $da = $this->parsePage($type,$ans);
//                var_dump($da);
                usleep(rand(5E6,15E6));
                $page++;
            }
            $this->curl_init();
        }


    }
}
