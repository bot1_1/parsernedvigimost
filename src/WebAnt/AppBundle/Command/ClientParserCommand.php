<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Command\NeagentParser;
use TesseractOCR;
use WebAnt\AppBundle\Command\MakeArchieve;


class ClientParserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('parser:parse:client')
            ->setDescription('parse phones as client')
            ->addArgument('arg1',null,'1st argument(not used yet)')
        ;
    }

    private $ch;
    private $em;
    private $triedlogin = false;
    private $key = '8t5hhx41'; // neagent.client.key
    private $proxies = array(
        '127.0.0.1:8118'
    );
    private function getApiKey(){
        $np = new NeagentParser();
        return $np->getApiKey();
    } // rucaptcha.apikey

    private function getRandProxy(){
        $i = array_rand($this->proxies);
        $proxy = $this->proxies[$i];
        return $proxy;
//        return $this->proxies[array_rand($this->$proxies)];
    }

    private function curl_init()
    {
        $this->ch = curl_init();

        //Handle cookies for the login
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, 'client_cookie.txt');
        curl_setopt($this->ch, CURLOPT_COOKIEJAR , 'client_cookie.txt');
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);

        //curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));

        //Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
        //not to print out the results of its query.
        //Instead, it will return the results as a string return value
        //from curl_exec() instead of the usual true/false.
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36");
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
    }

    private function login($url, $type){

        //http://neagent.info/rostov/login/

        curl_setopt($this->ch, CURLOPT_POST, 0);
        curl_setopt($this->ch, CURLOPT_URL, "http://neagent.info/rostov/login/");
//id_section=75&url=%2Frostov%2Fbase%2F4985719%2F&key=8p320c11
        $post = http_build_query(array(
            "id_section" => $type+74,
            "url" => $url,
            "key" => $this->key
        ));
        $this->triedlogin = true;
        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post);
        return curl_exec($this->ch);
    }


    private function getByOldId($old_id){
        $em = $this->getContainer()->get('doctrine')->getManager();
        $query = $em->createQuery(
            'SELECT p
                FROM WebAntAppBundle:Realty p
                WHERE p.old_id = :old_id'
        )->setParameter('old_id', $old_id);
        $res = $query->getResult();
        return count($res)?$res[0]:null;
    }


    private function request($type, $page){
        $Url = "http://neagent.info/rostov/arenda/$type/";
        if($page>1){
            $Url.="?page=$page";
        }
        curl_setopt($this->ch, CURLOPT_URL, $Url);

//        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());

        // ENABLE HTTP POST
//        curl_setopt($this->ch, CURLOPT_POST, 1);

        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array(
            'Accept-Language:ru,en;q=0.8'
        ));
        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());

        return curl_exec($this->ch);
    }


    private function parsePage($type, $page){
        $crawler = new Crawler;
        $crawler->addHTMLContent($page);

        $trs = $crawler->filter('.infoblock4');

        $da = true; // objects are actual

        if($trs->count()==0){
            print_r('пусто. попробуем ещё раз');
            usleep(rand(1E6,1E7));
            return $this->parsePage($type, $page);
        }


        foreach ($trs as $el) {

            $pq = new Crawler($el);
            $old_id = preg_replace("/[^0-9]/","",$pq->attr('id'));
            $stext = $pq->filter('#atitle')->text();


            $d = $pq->filter('#date')->text();
//            print_r("объект $old_id вижу $d создался\n");

            $arr = array("Сегодня","Вчера","Позавчера");
            if(in_array($d,$arr)){
                $days = array_search($d,$arr);
                $date = new \DateTime();
                $date->modify("-$days day");
                $info = array(
                    "date"=>$date,
                    "old_id"=>$old_id
                );
                $price = $pq->filter('#price')->text();
                $info["price"] = preg_replace("/[^0-9]/","",$price);
                $objects[] = $info;
            } else {
//                print_r("недвижимость №${info['old_id']} не актуальна, смотрим следующую категорию... \n");
                $da = false;
            }
        }
        $already = 0;

        if(isset($objects)){
            var_dump(count($objects));
            shuffle($objects);
            foreach($objects as $obj){
                $realty = $this->getByOldId($obj["old_id"]);
                if($realty){
                    $date  = new \DateTime();
                    $date  = $date->getTimestamp();
                    $date2 = $realty->getDate()->getTimestamp();
                    if( ($date-$date2)>100500 ||   // object parsed lots of times ago or
                         $realty->getPhone() == NULL // we don't have its phone
                        ){
                        $this->parseObject($type, $obj);
                        usleep(rand(15E6,35E6));
                    } else {
                        $id     = $realty->getId();
                        $old_id = $realty->getOldId();
                        print_r("объект №$old_id($id) уже парсился недавно, пропускаем\n");
                        usleep(rand(15E6,25E6));
                        $already++;
                    };
                } else {
                    $this->parseObject($type, $obj);
                    usleep(rand(15E6,35E6));
                }
            }
        } else {
            print_r("я ничего не вижу\n");
            return false;
        }
        if($already == count($objects)){
            $da = false;
            print_r("мы уже тут видели всё, переходим к следующей категории");
        }
        return $da;
    }


    private function getCaptcha(){
        curl_setopt($this->ch, CURLOPT_POST, 0);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        print_r("Получаем капчу...");

        $rand = rand(100500,999999);
        curl_setopt($this->ch, CURLOPT_URL, "http://neagent.info/usingcaptcha/captha_img_vh3/?r=$rand");

        $rand = rand(0,999999999);
        $filename = "tmp/captcha$rand.jpg";



        $fp = fopen($filename, 'wb');
        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());

        curl_setopt($this->ch, CURLOPT_FILE, $fp);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_exec($this->ch);
        fclose($fp);

        $realpath = realpath($filename);
        print_r(" пытаемся её расшифровать... ");

        $cap = new RuCaptcha();
        $apikey = $this->getApiKey();
        $result = $cap ->recognize($realpath,$apikey);
        unlink($filename);
        if($result == false){
            $result = '';
        }
        while(strlen($result)<5){
            $seed = str_split(
                'abcdefghijklmnopqrstuvwxyz'
                .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                .'0123456789');
            shuffle($seed);
            $result .= $seed[0];
        }
        if(strlen($result)>5){
            substr($result, 0, 5);
        }

        print_r("captcha = $result\n");
        curl_setopt($this->ch, CURLOPT_FILE, STDOUT);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        return $result;
    }


    private function getPhone($old_id, $h, $captcha){
        print_r("запрашиваем телефон...\n");
        curl_setopt($this->ch, CURLOPT_URL, "http://neagent.info/ajax/phone/img/userkey/");
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, 'id='.$old_id.'&h='.$h.'&captcha='.$captcha.'&rand='.(rand(1,999999)));
        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());

        $ans = curl_exec($this->ch);
        $y = strpos($ans, 'base64,' );
        if(substr($ans,0,3) == "cap"){
            $cap = $this->getCaptcha();
            usleep(rand(10E6,25E6));
            return $this->getPhone($old_id, $h, $cap);
        }

        $a = substr($ans, $y+7);
        $a = substr($a, 0, strpos ($a, '"'));
        $a = str_replace(' ','+',$a);
        $data = base64_decode($a);
        if(!$data){
            print_r("это не base64\n");
            file_put_contents("/tmp/answers", $a."\n", FILE_APPEND);
            return NULL;
        }

        $file = "web/phoneimgs/_$old_id.png";
        file_put_contents($file, $data);
        print_r('получили картинку  ');

        $tmpfile = "tmp/tmp_".rand(100500,999999).".png";
        shell_exec("convert $file -resize 300% -flatten $tmpfile");

        $ocr = new TesseractOCR($tmpfile);
        $ocr->setWhitelist(range(0,9), '-');
        $phone = $ocr->recognize();
        $phone = preg_replace('/\s+/', '', $phone);
        unlink($tmpfile);
        return $phone;
    }





    private function parseObject($type, $info0){
//        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());
        curl_setopt($this->ch, CURLOPT_POST, 0);
        curl_setopt($this->ch, CURLOPT_URL, "http://neagent.info/rostov/base/{$info0["old_id"]}/");
        curl_setopt($this->ch, CURLOPT_PROXY, $this->getRandProxy());

        $html = curl_exec($this->ch);

        $crawler = new Crawler;
        $crawler->addHTMLContent($html);
        $info= array();

        if($crawler->filter('#phone')->count()==0){
            print_r("телефона не видно");
            return $this->parseObject($type, $info0);
        }


        $ph = $crawler->filter('#phone')->text();
        if(preg_match("/скрыт/",$ph)){
            if(!$this->triedlogin){
                print_r("телефон, к сожаленью, скрыт. залогиниваемси\n");
                usleep(rand(3E6,7E6));
                $html = $this->login("http://neagent.info/rostov/base/{$info0["old_id"]}/", $type);
                $crawler = new Crawler;
                $crawler->addHTMLContent($html);
//                $info = array();
//                $phone = $crawler->filter('#phone h2')->text();
//                $info["phone"] = preg_replace("/[^0-9]/","",$phone);
//                print_r("телефон $phone\n");
            } else {
                print_r("прошлая попытка логина оказалась неудачной.\n");
            }
        } else {
//        print_r("уже залогинены, ура!\n");
//            $phone = $crawler->filter('#phone h2')->text();
//            $info["phone"] = preg_replace("/[^0-9]/","",$phone);
//            print_r("телефон $phone\n");
        }

//        if($crawler)

        if($crawler->filter('#phone h2')->count()>0){
            $phone = $crawler->filter('#phone h2')->text();
            $info["phone"] = preg_replace("/[^0-9]/","",$phone);
            print_r("телефон {$info["phone"]}\n");
        }

        if($crawler->filter('#phone #phoneshow')->count()>0){
            $phoneshow = $crawler->filter('#phone #phoneshow')->attr('onclick');
            $phoneshow = explode("'",$phoneshow)[3];
            usleep(rand(15E6,35E6));
            $info["phone"] = $this->getPhone($info0["old_id"],$phoneshow,'');
            print_r("телефон {$info["phone"]}\n");
        }

        if($crawler->filter('#tdcontent>h1')->count()){
            $info["stext"]   = $crawler->filter('#tdcontent>h1')->text();
        }
        if($crawler->filter('#tdcontent #textcontent')->count()){
            $info["text"] = $crawler->filter('#tdcontent #textcontent')->text();
            $info["text"] = preg_replace('#^\d+#', '', $info["text"]);
            $info["text"] = preg_replace( '/\s*\d+$/', '', $info["text"] );
        }
        if($crawler->filter('#tdcontent #bi_street>span')->count())
        {
            $info["street"]  = $crawler->filter('#tdcontent #bi_street>span')->eq(0)->text();
        }

        if($crawler->filter('#tdcontent #distr')->count())
        {
            $distr   = $crawler->filter('#tdcontent #distr')->html();
            $info["distr"]   = explode('<', $distr)[0];
        }


        $area = "0";
        $n = $crawler->filter('table.baseinfo tr')->count();
        if($n > 2){
            $a = $crawler->filter('table.baseinfo tr')->eq(2)->text();
            if (strpos($a,'Общая площадь') !== false) {
                $area = $crawler->filter('table.baseinfo tr')->eq(2)->text();
            }
            $a = $crawler->filter('table.baseinfo tr')->eq(3)->text();
            if (strpos($a,'Общая площадь') !== false) {
                $area = $crawler->filter('table.baseinfo tr')->eq(3)->text();
            }
            $area = preg_replace("/[^0-9.,]/","",$area);
            $info["area"] = preg_replace("/,/",".",$area);
        } else {
            print_r("походу страница пуста");
            return $this->parseObject($type, $info0);
        }



        $obj = $this->getByOldId($info0["old_id"]);
        if(!isset($obj)){
            $realty = new Realty();
            $exist = false;
        } else {
            $realty = $obj;
            $exist = true;
        }

        $realty->setType($type);
        $realty->setWho('Парсер(клиент)');
        $realty->setOldId($info0["old_id"]);
        $realty->setStext($info["stext"]);
        $realty->setText($info["text"]);
        if(array_key_exists("street",$info)){
            $realty->setAddr($info["street"]);
        }
        if(array_key_exists("phone",$info)){
            $realty->setPhone($info["phone"]);
        }
        $realty->setDistr($info["distr"]);
        $realty->setPrice($info0["price"]);
        $realty->setArea($info["area"]);
        $status = $realty->getStatus();
        if($status == 5){
            $realty->setStatus(0);
        }

        $old_id = $realty->getOldId();
        $em = $this->getContainer()->get('doctrine')->getManager();

        if($exist){
            $id = $realty->getId();
            print_r("недвижимость №$old_id уже есть в базе(id=$id), обновляем\n");
            $em->merge($realty);
            $em->flush();
            $em->clear();
        }

        if(!$exist){
            print_r("недвижимость №$old_id новая, записываем.\n");
            $em->persist($realty);
            $em->flush();
            $em->clear();
        }
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->proxies = $this->getContainer()->getParameter('proxies');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $mk = new MakeArchieve();
        $mk->doIt($em);

        $types = array(
            "sdam-komnatu",
            "sdam-odno-komnatnuyu-kvartiru",
            "sdam-dvuh-komnatnuyu-kvartiru",
            "sdam-treh-komnatnuyu-kvartiru",
            "",
            "sdam-dom",
        );
        $this->proxies = $this->getContainer()->getParameter('proxies');
        $this->curl_init();


        $nums = [0,1,2,3,5];
        shuffle($nums);


//        $type = 1;
        foreach($nums as $type){
            $page = 1;
            $da = true;
            while($da){
                print_r("parse page $page of type=${types[$type]}\n");
                $ans = $this->request($types[$type],$page);
                try{
                    $da = $this->parsePage($type,$ans);
                } catch (Exception $e) {
                    print_r("\n ошибка при парсинге страницы");
                    echo $e->getMessage();
                }
                usleep(rand(40E6,75E6));
                $page++;
            }
        }
    }
}


/*
 * http://openstat.net/cnt?cid=2185073
 * &p=8
 * &pg=http%3A%2F%2Fneagent.info%2Frostov%2Fbase%2F5309741%2F
 * &pvid=b8eecf1c-2534-42ab-b9d7-9621bfcf8209
 * &name=mousemove
 * &data=%7B%22duration%22%3A193%2C%22distance%22%3A340%7D
 * &rn=0.9690213803365828
 * */
/*
 * http://top-fwz1.mail.ru/tracker
 * ?js=13;id=2034514;e=RT/beat;sid=f047f8f1;ids=2034514;ver=60;_=0.5984729666428203
 * */



/*
 *
 * POST http://neagent.info/ajax/phone/img/userkey/
 * formdata id=5362195&h=b1be38bf2abbb27ee60e40eee9034d5e&captcha=&rand=416552
 * captcha
 *
 * GET http://neagent.info/usingcaptcha/captha_img_vh3/?r=871602
 *
 *
 *
 * id=5362195&h=b1be38bf2abbb27ee60e40eee9034d5e&captcha=skEJT&rand=544485
 * POST http://neagent.info/ajax/phone/img/userkey/
 * <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF8AAAAMEAQAAAD+z3VkAAAAAmJLR0T//xSrMc0AAAAJcEhZcwAAAEgAAABIAEbJaz4AAANeSURBVFjD7ZdPSJNxGMc/Ttlcc403ZLSgwEFFUoMOdZIdhc5dIgQr0C5eOkSeRpiZYjt0zbBDiroKJuklixXskkgQQQwhwcvCRZS6UlNch4eX9323394/5/zCj72/P8/z/T7P7+8AgGPHIJuFchk2N+H5c4hGUSIchmfPZOz37/DkCRw9ah0TCsHqqnc7HXZ67Px44TAjmYRKxaifOAGzs8K9uQmZTH0/587B27fw+7fwZjKgadKnaTA3J+2/ftXxk8vBjRvQ3Cyltxdev1aTpdMQj0NTEwSD0N8P+bzRHwjAq1fWYNzYudVj58cLhxlzc1a9i4tw6xZEIlLu34c3b9S2hQJcvQotLVLSaVksAAMD8OiRLMZwGEZGYHCwysHOjgSpIxiE7W012dqatX74sKwOHfk8nD1bm3wnO7d67Px44dBx4QJMTNTqNSMQgD9/cIVIxBibz0MiYfS1tSkmMZeD69eFxO+H7m54907tvFyWmdShabC3Z9QvXZLf6mCc7NzqsfPjhUNHNgvnz9dPfjgsO2962l3yW1uhWJTvUknsdRw6JG0WnD4NGxsioFKRIE6dUjufmYHbt8WRpsHjx7C/XzuuOhi3dk567Px44QA5rxcW1HoBXr6Enz/lPE8m3SW/p0eOG4C/f8HnM/p8PsVimJ2FO3dke7e0QColgaigafDiBWxtwcoKdHWpj6jqYNzaOemx8+OFA2Bqqv5ONSdsdBQ+f3ZOfDQqO8nvl/rurrW/oUGOVAvKZev2iERk5emi9KJCIlH7srELRmVXzWGnxy2/E8fJk/Dpkzu94bCRtHr58PthchJiMaOtVLLeXYGA+V7yGTNkvpj2943t2tBgFBXa2+HDBzzDbFfNYafHC78dx+ioTI45kfUmIBCA9fX6+YjH4elTuHcPvn0z2gsF69MyEpE2C8bG4OZNOHLEeKKNjamFLC/DtWsiqL1d3rgdHbXjqgNxa+ekx86PFw47vV++wOXL0Ngox97QEAwPq+06O2FpCY4fr+1LpeDBA3kENDdDXx/cvVs1KBSC8XHZ2hsb8m1+NZhx8SJ8/Cir8+tXuHLFORgvdk567Px44bDTe+aMTNz2Nvz4AQ8fyn8HFYpF61Fk3kmxGLx/L3dQuQzz88YfsAMc4H/FP0SG+pZT1/KOAAAAAElFTkSuQmCC">
 *
 *
 *
        $np = new NeagentParser();

$x = '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAF8AAAAMEAQAAAD+z3VkAAAAAmJLR0T//xSrMc0AAAAJcEhZcwAAAEgAAABIAEbJaz4AAANeSURBVFjD7ZdPSJNxGMc/Ttlcc403ZLSgwEFFUoMOdZIdhc5dIgQr0C5eOkSeRpiZYjt0zbBDiroKJuklixXskkgQQQwhwcvCRZS6UlNch4eX9323394/5/zCj72/P8/z/T7P7+8AgGPHIJuFchk2N+H5c4hGUSIchmfPZOz37/DkCRw9ah0TCsHqqnc7HXZ67Px44TAjmYRKxaifOAGzs8K9uQmZTH0/587B27fw+7fwZjKgadKnaTA3J+2/ftXxk8vBjRvQ3Cyltxdev1aTpdMQj0NTEwSD0N8P+bzRHwjAq1fWYNzYudVj58cLhxlzc1a9i4tw6xZEIlLu34c3b9S2hQJcvQotLVLSaVksAAMD8OiRLMZwGEZGYHCwysHOjgSpIxiE7W012dqatX74sKwOHfk8nD1bm3wnO7d67Px44dBx4QJMTNTqNSMQgD9/cIVIxBibz0MiYfS1tSkmMZeD69eFxO+H7m54907tvFyWmdShabC3Z9QvXZLf6mCc7NzqsfPjhUNHNgvnz9dPfjgsO2962l3yW1uhWJTvUknsdRw6JG0WnD4NGxsioFKRIE6dUjufmYHbt8WRpsHjx7C/XzuuOhi3dk567Px44QA5rxcW1HoBXr6Enz/lPE8m3SW/p0eOG4C/f8HnM/p8PsVimJ2FO3dke7e0QColgaigafDiBWxtwcoKdHWpj6jqYNzaOemx8+OFA2Bqqv5ONSdsdBQ+f3ZOfDQqO8nvl/rurrW/oUGOVAvKZev2iERk5emi9KJCIlH7srELRmVXzWGnxy2/E8fJk/Dpkzu94bCRtHr58PthchJiMaOtVLLeXYGA+V7yGTNkvpj2943t2tBgFBXa2+HDBzzDbFfNYafHC78dx+ioTI45kfUmIBCA9fX6+YjH4elTuHcPvn0z2gsF69MyEpE2C8bG4OZNOHLEeKKNjamFLC/DtWsiqL1d3rgdHbXjqgNxa+ekx86PFw47vV++wOXL0Ngox97QEAwPq+06O2FpCY4fr+1LpeDBA3kENDdDXx/cvVs1KBSC8XHZ2hsb8m1+NZhx8SJ8/Cir8+tXuHLFORgvdk567Px44bDTe+aMTNz2Nvz4AQ8fyn8HFYpF61Fk3kmxGLx/L3dQuQzz88YfsAMc4H/FP0SG+pZT1/KOAAAAAElFTkSuQmCC">';
$y = strpos ( $x , 'base64,' );
$a = substr($x, $y+7);
$a = substr($a, 0, strpos ($a, '"'));
$a = str_replace(' ','+',$a);
$data = base64_decode($a);
file_put_contents('42.png', $data);




 * */


