<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use WebAnt\AppBundle\Service\SmsService;
use Symfony\Component\DomCrawler\Crawler;
use TesseractOCR;


class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('test:command')
            ->setDescription('test command, use for checking some functions')
            ->addArgument('arg1',null,'1st argument(not used yet)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $ocr = new TesseractOCR('web/phoneimgs/__');
//        $ocr->setWhitelist(range(0,9), '-');
//        $phone = $ocr->recognize();
//        print_r($phone);

        $dummy = false;
        $arg1 = $input->getArgument('arg1');
        if($arg1 == 'dummy'){
            $dummy = true;
        }
        var_dump($dummy);







//        $em = $this->getContainer()->get('doctrine')->getManager();
//        $query = $em->createQuery(
//            'SELECT p
//                FROM WebAntAppBundle:Realty p
//                WHERE p.id = :id'
//        )->setParameter('id', 8858);
//        $res = $query->getResult();
//
////        var_dump($res[0]);
//        $res[0]->setStatus(42);
//        $em->clear();
//        $em->merge($res[0]);
//        $em->flush();






//        $crawler = new Crawler;
//        $crawler->addHTMLContent('');
//        $a = $crawler->filter('#phone h2')->count();
//        var_dump($a);


//        $sms = new SmsService();
//        $sms->sendSms('79885654799','');
    }
}
