<?php
namespace WebAnt\AppBundle\Command;


use WebAnt\AppBundle\Entity\Realty;

class MakeArchieve
{

    function __construct()
    {
    }


    public function doIt($em){
//        $em = $this->container->get('doctrine');
        //getting objects for parsing:
        $date = new \DateTime();
        $date->modify('-30 day');


//        $query = $em->createQuery(
//            'SELECT p
//                FROM WebAntAppBundle:Realty p
//                WHERE p.date < :date
//                '
//        )->setParameter(':date', $date);
//        $result = $query->getResult();
        //:objects.get

        $query = $em->createQuery(
            'UPDATE WebAntAppBundle:Realty p
                SET p.status = 0
                WHERE p.date > :date
                '
        )->setParameter(':date', $date);
        $result = $query->getResult();

        $em->flush();
//        $em->close();



    }


}

