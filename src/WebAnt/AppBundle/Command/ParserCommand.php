<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Command\NeagentParser;



class ParserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('parser:parse')
            ->setDescription('parse Realtis')
            ->addArgument('arg1',null,'1st argument(not used yet)')
        ;
    }

    private $ch;


    private function request($ch, $page, $s){

        $data = array(
            'page' => $page,
            'ord' => 'date',
            'sord' => 'desc',
            'limit' => '40',
            'rnd' => 456245
        );

//        $s = 75;
        $limit = 40;
        $data2 = http_build_query($data);
        //	var_dump($data2);


        $Url = 'http://neagent.info/client/base/?'.$data2;
        //Set the URL to work with
        //var_dump($Url);




        curl_setopt($ch, CURLOPT_URL, $Url);

        // ENABLE HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);

        //Set the post parameters
        curl_setopt($ch, CURLOPT_POSTFIELDS, 's='.$s.'&limit='.$limit);

        return curl_exec($ch);
    }



    private function getByOldId($old_id){
        $realty = new Realty();
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($realty);
        $query = $em->createQuery(
            'SELECT p
                FROM WebAntAppBundle:Realty p
                WHERE p.old_id = :old_id'
        )->setParameter('old_id', $old_id);
        $res = $query->getResult();
        return count($res)?$res[0]:null;
    }


    private function doesExist($old_id){
        return $this->getByOldId($old_id);
    }


    private function parse($ch, $page, $s){

        $result = $this->request($ch,$page, $s);
        preg_match("/<tbody>(.*)<\/tbody>/s",$result, $results);
        if(!count($results)){
            return false;
        }


        //$document = phpQuery::newDocument($results[0]);
        $crawler = new Crawler;
        $crawler->addHTMLContent($results[0]);
//        $crawler->addHTMLContent(file_get_contents('src/WebAnt/AppBundle/Command/html_prettified.html'), 'UTF-8');


        $trs = $crawler->filter('tbody > tr');



        foreach ($trs as $el) {
            $pq= new Crawler($el);

            if(!$this->doesExist($pq->attr('id_noagent'))){
                $realty = new Realty();
                $realty->setType($s - 74);
                $realty->setWho('Парсер(манагер)');
                $realty->setOldId(   $pq->attr('id_noagent'));
//                $realty->setDateUpdate(new \DateTime($pq->filter('td[id=d_upd]')->attr('abbr'))  );
                $realty->setPrice(   $pq->filter('td[id=d_price]')->attr('abbr'));
//            $realty->setPriceOur($pq->filter('td[id=d_price]')->attr('abbrour'));
                $realty->setArea(    $pq->filter('td[id=d_area]')->attr('abbr'));
                $realty->setDistr(   $pq->filter('td[id=distr]')->attr('abbr'));
                $realty->setStreet(  $pq->filter('td[id=street]')->attr('abbr'));
//                var_dump($pq->attr('id_noagent'));


                $func = $pq->filter('a[id=phoneshow]')->attr('onclick');
                $ee = explode("'",$func);

                if(count($ee)>3){
                    $e = $ee[3];
                    $realty->setPhoneShow($e);
                    print_r(count($func));
                } else {
                    print_r("\n");
                    print_r(count($func));
                    print_r("\n");
                }




                $addr_td = $pq->filter('td[id=street] a.ymaplink');
                if($addr_td->count()){
                    $realty->setAddr($addr_td->text());
                }

                $realty->setStext($pq->filter('td[id=content] p[id=title] a')->text());
                $realty->setText( $pq->filter('td[id=content] p[id=ct]')->text());

                $em = $this->getContainer()->get('doctrine')->getManager();
                $em->clear();
                $em->persist($realty);
                $em->flush();
                $em->clear();

            } else {




//                $realty = $this->getByOldId($pq->attr('id_noagent'));
//                $em = $this->getContainer()->get('doctrine')->getManager();
//
//                $func = $pq->filter('a[id=phoneshow]')->attr('onclick');
//                $ee = explode("'",$func);
//
//                if(count($ee)>3){
//                    $e = $ee[3];
//                    $realty->setPhoneShow($e);
//                } else {
//                    print_r($func);
//                    print_r("\n");
//                }
//                $em->flush();
//                $em->clear();

                //print_r("object_already_exists\n");
                //return false;
            }

//            $data = array(
//                'old_id'  => $pq->attr('id_noagent'),
//                'date'    => $pq->filter('td[id=d_upd]')->attr('abbr'),
//                'price'   => $pq->filter('td[id=d_price]')->attr('abbr'),
//                'priceour'=> $pq->filter('td[id=d_price]')->attr('abbrour'),
//                'area'    => $pq->filter('td[id=d_area]')->attr('abbr'),
//                'distr'   => $pq->filter('td[id=distr]')->attr('abbr'),
//                'street'  => $pq->filter('td[id=street]')->attr('abbr'),
////                'addr'    => $pq->filter('td[id=street] a.ymaplink')->text(),
//                'stext'   => $pq->filter('td[id=content] p[id=title] a')->text(),
//                'text'    => $pq->filter('td[id=content] p[id=ct]')->text()
//            );
//            print_r($data);
//            echo("\n");
        }
        return true;
    }




    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $np = new NeagentParser();

        $ch = $np->init();




        $arg1 = $input->getArgument('arg1');


//        $this->parse(0,0,0);
        $ch = $np->login($ch);
//        usleep(rand(3E6,9E6));

//                $da = $this->parse($ch,$page,74+$type);

        foreach (array(0,1,2,3,5) as $type){

            $maxpage= array(13,14,11,6,0,8)[$type];
            for($page=0;$page<$maxpage;$page++){
                print_r("waiting... ");
                sleep(rand(30,70));
                usleep(rand(0,1E7));
                print_r("parsing page $page of type $type\n");
                $da = $this->parse($ch,$page,74+$type);
            }


//            $page=0;
//            $da = true;
//            while($da){
//                print_r("parsing page $page of type $type\n");
//                $da = $this->parse($ch,$page,74+$type);
//                $page+=1;
//            }

        }

        usleep(rand(0,1E7));
        sleep(rand(15,50));
        usleep(rand(3E6,9E6));
        $np->logout($ch);
    }
}