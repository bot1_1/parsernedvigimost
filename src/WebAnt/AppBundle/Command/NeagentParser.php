<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Component\DomCrawler\Crawler;
use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Command\RuCaptcha;

class NeagentParser
{

    private $apikey = "f2c071daa7707a9619de57f8220051f2";//
    public function getApiKey(){
        return $this->apikey;
    } // rucaptcha.apikey

    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        //init curl
        $ch = curl_init();

        //Handle cookies for the login
        curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie.txt');
        curl_setopt($ch, CURLOPT_COOKIEJAR , 'cookie.txt');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));

        //Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
        //not to print out the results of its query.
        //Instead, it will return the results as a string return value
        //from curl_exec() instead of the usual true/false.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        return $ch;
    }



    private function capOnLogin($ch){

        curl_setopt($ch, CURLOPT_POST, 0);
        $rand = rand(100500,999999);
        curl_setopt($ch, CURLOPT_URL, "http://neagent.info/usingcaptcha/captha_img_3/$rand");

        $filename = 'tmp/captcha.jpg';

        $fp = fopen($filename, 'wb');

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        fclose($fp);

        $realpath = realpath($filename);

        $cap = new RuCaptcha();
        $apikey = $this->getApiKey();
        $result = $cap ->recognize($realpath,$apikey);
        unlink($filename);

        curl_setopt($ch, CURLOPT_FILE, STDOUT);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        return $result;
    }



    public function login($ch, $captcha = "") {

        $username = 'niki_1605%40icloud.com';
        $password = 'DIilove11';
        $action = 'login';
        $addl = 'Shockwave+Flash++%3A%3A%3A+Chromoting+Viewer++%3A%3A%3A+';
        $loginUrl = 'http://neagent.info/client/login/';
        //Set the URL to work with
        curl_setopt($ch, CURLOPT_URL, $loginUrl);


        // ENABLE HTTP POST
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        //Set the post parameters
        if($captcha){
            print_r("\ncaptcha=$captcha\n");
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'login='.$username.'&password='.$password.'&captchacode='.$captcha.'&action='.$action.'&addl='.$addl);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'login='.$username.'&password='.$password.'&action='.$action.'&addl='.$addl);
        }

        $result = curl_exec($ch);

        $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        var_dump($last_url);
        if($last_url!="http://neagent.info/client/home/"){
            $captcha = $this->capOnLogin($ch);
            $this->login($ch,$captcha);
        }
        //http://neagent.info/client/?error=setcaptcha
        //http://neagent.info/client/home/
        return $ch;
    }



    public function logout($ch) {
        curl_setopt($ch, CURLOPT_URL, 'http://neagent.info/client/logout/');
        //curl_setopt($ch, CURLOPT_GET, 1);
        curl_exec($ch);
        return $ch;
    }

}

