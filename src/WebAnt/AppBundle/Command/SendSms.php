<?php
namespace WebAnt\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Entity\User;
use WebAnt\AppBundle\Entity\UserInfo;
use WebAnt\AppBundle\Data\Data;

use WebAnt\AppBundle\Service\UserRealtisService;
use WebAnt\AppBundle\Service\SmsService;


class SyncUserRealtisCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('sms:adv_user:send')
            ->setDescription('send sms to users')
            ->addArgument('arg1',null,'1st argument(not used yet)')
        ;
    }

    private $em;


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $svc = new UserRealtisService();
        $sms = new SmsService();
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        $query = $this->em->createQuery(
            'SELECT u
                 FROM WebAntAppBundle:User u
                 WHERE u.roles = :roles
                '
        )->setParameter(':roles', 'a:1:{i:0;s:13:"ROLE_ADV_USER";}');
        $users = $query->getResult();
        foreach($users as $u){

        }


    }
}
