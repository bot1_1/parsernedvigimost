<?php

namespace WebAnt\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\UserBundle\SonataUserBundle;
use WebAnt\AppBundle\Data\Data;


class MapController extends Controller
{
    /**
     * @Route("/map", name="map")
     */
    public function indexAction()
    {

//        return new Response('Hello world!');
//        return new RedirectResponse('admin/dashboard');
        $data=array();
        $data['navigation'][] = array('href'=>'login'          , 'caption'=>'войти');
        $data['navigation'][] = array('href'=>'admin/dashboard', 'caption'=>'сайт');

        $data['navigation'][1]['submenu'][] = array('href'=>'admin/webant/app/realty/list', 'caption'=>'список недвижимости');
        $data['navigation'][1]['submenu'][] = array('href'=>'admin/webant/app/user/list'  , 'caption'=>'список пользователей');
        $data['navigation'][1]['submenu'][] = array('href'=>'admin/adv_user/list'         , 'caption'=>'список рекламных пользователей');



        return $this->render('default/map.html.twig',$data);
    }
}
