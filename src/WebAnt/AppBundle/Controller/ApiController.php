<?php
/**
 * Created by PhpStorm.
 * User: kwant
 * Date: 08.07.15
 * Time: 16:23
 */
namespace WebAnt\AppBundle\Controller;

use WebAnt\AppBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use WebAnt\AppBundle\Entity\Realty;

class ApiController extends FOSRestController {

    /**
     * @Route("/api/v1/qwer", name="app")
     */
    public function listAction()
    {
        $date = new \DateTime();
        $date->modify('-3 day');
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT r
                FROM WebAntAppBundle:Realty r
                WHERE r.date > :date
                '
        )->setParameter('date', $date);
        $objects = $query->getResult();

        $serializer = $this->container->get('serializer');

        // serialize all of your entities
        $serializedCities = array();
        foreach ($objects as $r) {
            $str = $serializer->serialize($r, 'json');
            $resp[] = json_decode($str);
        }
        return new JsonResponse($resp);
//        return new JsonResponse(array());
    }


    /**
     * @Route("/api/v1/sav__obj", name="save")
     */
    public function saveAction(Request $request)
    {
        $req = new Request();

        $obj = json_decode($req->getContent(),true);
//
//        if($req->query->get('secret')!='siili'){
//            return new Response($req->get('secret'));
//        }
//

        if($request->query->get('secret')!='siili'){
            return new Response("");
        }
        if(!is_array($obj)){
            return new Response("");
        }

//        $em = $this->getContainer()->get('doctrine')->getManager();
//        $query = $em->createQuery(
//            'SELECT p
//                FROM WebAntAppBundle:Realty p
//                WHERE p.old_id = :old_id'
//        )->setParameter('old_id', $old_id);
//        $res = $query->getResult();


        $old_id = $obj["old_id"];

        $em = $this->get('doctrine')->getManager();
        $query = $em->createQuery(
            'SELECT p
                FROM WebAntAppBundle:Realty p
                WHERE p.old_id = :old_id'
        )->setParameter('old_id', $old_id);
        $res = $query->getResult();

        if(count($res)){
            $realty = $res[0];
            $exist = true;
        } else {
            $realty = new Realty();
            $exist = false;
        }


        $realty->setOldId($old_id);
//        $realty->setWho('Парсер(клиент)');
        $status = $realty->getStatus();
        if($status == 5 || !$status){
            $realty->setStatus(0);
        }



//        $serializer = $this->container->get('serializer');
//        $serializer->deserialize(json_encode($obj), '\WebAntAppBundle\Realty', 'xml',  array('object_to_populate' => $realty));

        foreach ($obj as $property => $value) {
            $val = str_replace(' ', '', ucwords(str_replace('_', ' ', $property)));
            $method = sprintf('set%s', $val); // or you can cheat and omit ucwords() because PHP method calls are case insensitive

            $realty->$method($value);
        }


        if($exist){
            $id = $realty->getId();
            print_r("недвижимость №$old_id уже есть в базе(id=$id), обновляем\n");
            $em->merge($realty);
            $em->flush();
            $em->clear();
        }


        if(!$exist){
            print_r("недвижимость №$old_id новая, записываем.\n");
            $em->persist($realty);
            $em->flush();
            $em->clear();
        }


//        $em = $this->getContainer()->get('doctrine')->getManager();
//        $query = $em->createQuery(
//            'SELECT p
//                FROM WebAntAppBundle:Realty p
//                WHERE p.old_id = :old_id'
//        )->setParameter('old_id', $old_id);
//        $res = $query->getResult();




        return new Response("");
//        return new JsonResponse(array());
    }







}