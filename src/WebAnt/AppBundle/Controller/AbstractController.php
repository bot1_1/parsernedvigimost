<?php

namespace WebAnt\AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class AbstractController extends FOSRestController
{
    protected        $objectClass;
    protected        $objectKey  = 'id';
    static protected $entityPath = 'WebAnt\AppBundle\Entity\\';

    protected function getObjectRepository()
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository($this->objectClass);
    }

    /*
     * Проверка на валидность JSON
     */
    protected function checkJson(Request $request)
    {
        if ('json' !== $request->getContentType()) {
            throw new HttpException(400, 'Invalid content type');
        }

        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE || $request->getContent() == '') {
            throw new HttpException(400, 'Invalid content type');
        }

        return $data;
    }

    protected function throwErrorIfNotValid($entity)
    {
        $validator = $this->get('validator');
        $errors    = $validator->validate($entity);
        if (count($errors) > 0) {
            throw new HttpException(400, 'Bad request (' . print_r($errors, true) . ').');
        }

        return true;
    }

    /*
     * Получить объект по ключу
     *
     *
     * @param string $keyValue
     *
     * @return Object
     *
     */
    public function getObjectAction($keyValue, $findArray = [])
    {
        $repository                  = $this->getObjectRepository();
        $findFunction                = 'findOneBy';
        $findArray[$this->objectKey] = $keyValue;
        $object                      = $repository->$findFunction($findArray);
        if (!$object) {
            throw new HttpException(404, 'No object this key (' . $keyValue . ').');
        }

        return $object;
    }

    /*
     * Получить списо объектов
     *
     *
     * @param string $request - Запрос
     * @param array $findArray - Масси параметров поиска
     *
     * @return Object
     *
     */
    public function getListObjectAction(Request $request, $findArray = [])
    {
        $repository = $this->getObjectRepository();

        $reflect    = new \ReflectionClass($this->objectClass);
        $properties = $reflect->getProperties();
        $orderArray = [];
        $limit      = null;
        $offset     = null;

        foreach ($properties as $prop) {
            if ($request->query->get($prop->getName())) {
                $findArray[$prop->getName()] = $request->query->get($prop->getName());
            }
            if ($request->query->get("orderby") == $prop->getName()) {
                $orderArray[$prop->getName()] = 'ASC';
            } elseif ($request->query->get("orderbydesc") == $prop->getName()) {
                $orderArray[$prop->getName()] = 'DESC';
            }
        }
        if (preg_match('/^[0-9]+$/', $request->query->get("limit"))) {
            $limit = (int)$request->query->get("limit");
        }
        if (preg_match('/^[0-9]+$/', $request->query->get("start")) &&
            preg_match('/^[0-9]+$/', $request->query->get("limit"))
        ) {
            $offset = (int)$request->query->get("start");
        }

        $objects = $repository->findBy($findArray, $orderArray, $limit, $offset);
        if (!$objects) {
            $object['items'] = [];
            $object['count'] = 0;

            return $object;
        }
        $count           = count($repository->findBy($findArray));
        $object['items'] = $objects;
        $object['count'] = $count;

        return $object;
    }

    /*
    * Удалить объект
    */
    public function  deleteObjectAction($keyValue, $arrayClass = [], $arrayField = [], $arrayCallBack = [], $beforeFunction = null, $afterFunction = null)
    {
        //если массивы не равны
        if (!(sizeof($arrayClass) === sizeof($arrayField) && sizeof($arrayClass) === sizeof($arrayCallBack))
        ) {
            throw new HttpException(400, 'Error');
        }

        $em           = $this->getDoctrine()->getManager();
        $repository   = $this->getObjectRepository();
        $findFunction = 'findOneBy' . ucfirst($this->objectKey);
        $object       = $repository->$findFunction($keyValue);

        if (!$object) {
            throw new HttpException(404, 'Objects not found');
        }

        if (!is_null($beforeFunction)) {
            call_user_func($beforeFunction, $object);
        }

        $object->Del = true;

        //ищем зависимые объекты
        $count_class = count($arrayClass);
        for ($i = 0; $i < $count_class; $i++) {
            $items = $em->getRepository($arrayClass[$i])->findBy([$arrayField[$i] => $keyValue]);
            if ($items) {
                $callBack = $arrayCallBack[$i];
                foreach($items as $item){
                    call_user_func($callBack, $em, $item, $object);
                }
            }
        }

        if ($object->Del) {
            $em->remove($object);
        }

        try {
            $em->flush();
            if (!is_null($afterFunction)) {
                call_user_func($afterFunction);
            }
        } catch
        (\Exception $e) {
            throw new HttpException(400, 'Error with Database ' . $e->getMessage());
        }

        return ['ms' => 'ok'];
    }

    public function createObjectAction($requestArray, $arrayRequestName = [], $arrayClass = [], $arrayField = [])
    {
        //если массивы не равны
        if (!(sizeof($arrayRequestName) === sizeof($arrayClass) && sizeof($arrayClass) === sizeof($arrayField))) {
            throw new HttpException(400, 'Error');
        }

        $em         = $this->getDoctrine()->getManager();
        $reflect    = new \ReflectionClass($this->objectClass);
        $properties = $reflect->getProperties();
        $object     = new $this->objectClass();

        //проверяем существование зависимых объектов
        for ($i = 0; $i < sizeof($arrayRequestName); $i++) {
            if (isset($requestArray[$arrayRequestName[$i]])) {
                if ($requestArray[$arrayRequestName[$i]] == 0) {
                    $objects[$arrayField[$i]] = 0;
                } else if (isset($requestArray[$arrayRequestName[$i]])) {
                    $repository   = $em->getRepository($arrayClass[$i]);
                    $findFunction = 'findOneById';
                    $subObject    = $repository->$findFunction($requestArray[$arrayRequestName[$i]]);

                    if (!$subObject) {
                        throw new HttpException(400, 'No object (' . $arrayField[$i] . ').');
                    }

                    $objects[$arrayField[$i]] = $subObject;
                }
            }
        }

        //устанавливаем значения
        foreach ($properties as $prop) {
            $setter = 'set' . ucfirst($prop->getName());

            $valueRequest = (isset($requestArray[$prop->getName()])) ? $requestArray[$prop->getName()] : null;//$request->get($prop->getName());
            $valueObject  = (isset($objects[$prop->getName()])) ? $objects[$prop->getName()] : null;


            if (isset($valueObject) && $reflect->hasMethod($setter)) {
                if ($valueObject === 0) {
                    $object->$setter(null);
                } else {
                    $object->$setter($valueObject);
                }
            } else if (isset($valueRequest) && $reflect->hasMethod($setter) && !in_array($prop->getName(), $arrayField)
            ) {
                $object->$setter($valueRequest);
            }
        }

        //запуск валидатора
        $this->throwErrorIfNotValid($object);

        $em->persist($object);
        try {
            $em->flush();
        } catch
        (\Exception $e) {
            throw new HttpException(400, 'Error with Database ' . $e->getMessage());
        }

        return $object;

    }

    public function updateOrCreateObjectAction($requestArray, $keyValue, $arrayRequestName = [], $arrayClass = [], $arrayField = [])
    {
        if (!is_array($requestArray)) {
            throw new HttpException(400, 'Error call method');
        }

        //если массивы не равны
        if (!(sizeof($arrayRequestName) === sizeof($arrayClass) && sizeof($arrayClass) === sizeof($arrayField))) {
            throw new HttpException(400, 'Error');
        }


        $repository   = $this->getObjectRepository();
        $findFunction = 'findOneBy' . ucfirst($this->objectKey);
        $object       = $repository->$findFunction($keyValue);

        if (!$object) {
            $object = new $this->objectClass();
        }

        $em         = $this->getDoctrine()->getManager();
        $reflect    = new \ReflectionClass($this->objectClass);
        $properties = $reflect->getProperties();


        //проверяем существование зависимых объектов
        for ($i = 0; $i < sizeof($arrayRequestName); $i++) {
            if ($requestArray[$arrayRequestName[$i]] == 0) {
                $objects[$arrayField[$i]] = 0;
            } else if (isset($requestArray[$arrayRequestName[$i]])) {
                $repository   = $em->getRepository($arrayClass[$i]);
                $findFunction = 'findOneById';
                $subObject    = $repository->$findFunction($requestArray[$arrayRequestName[$i]]);

                if (!isset($subObject)) {
                    throw new HttpException(400, 'No object (' . $arrayField[$i] . ').');
                }

                $objects[$arrayField[$i]] = $subObject;
            }
        }

        //устанавливаем значения
        foreach ($properties as $prop) {
            $setter = 'set' . ucfirst($prop->getName());

            $valueRequest = (isset($requestArray[$prop->getName()])) ? $requestArray[$prop->getName()] : null;//$request->get($prop->getName());
            $valueObject  = (isset($objects[$prop->getName()])) ? $objects[$prop->getName()] : null;

            if (isset($valueObject) && $reflect->hasMethod($setter)) {
                if ($valueObject === 0) {
                    $object->$setter(null);
                } else {
                    $object->$setter($valueObject);
                }
            } else if (isset($valueRequest) && $reflect->hasMethod($setter) && !in_array($prop->getName(), $arrayField)
            ) {
                $object->$setter($valueRequest);
            }
        }

        $this->throwErrorIfNotValid($object);

        $em->persist($object);
        try {
            $em->flush();
        } catch
        (\Exception $e) {
            throw new HttpException(400, 'Error with Database ' . $e->getMessage());
        }

        return $object;
    }

    public function updateObjectAction($requestArray, $keyValue, $arrayRequestName = [], $arrayClass = [], $arrayField = [], $beforeFunction = null, $afterFunction = null)
    {
        if (!is_array($requestArray)) {
            throw new HttpException(400, 'Error call method');
        }

        //если массивы не равны
        if (!(sizeof($arrayRequestName) === sizeof($arrayClass) && sizeof($arrayClass) === sizeof($arrayField))) {
            throw new HttpException(400, 'Error');
        }


        $repository   = $this->getObjectRepository();
        $findFunction = 'findOneBy' . ucfirst($this->objectKey);
        $object       = $repository->$findFunction($keyValue);

        if (!isset($object)) {
            throw new HttpException(404, 'No object this key (' . $keyValue . ').');
        }

        if (!is_null($beforeFunction)) {
            call_user_func($beforeFunction, $object);
        }

        $em         = $this->getDoctrine()->getManager();
        $reflect    = new \ReflectionClass($this->objectClass);
        $properties = $reflect->getProperties();


        //проверяем существование зависимых объектов
        for ($i = 0; $i < sizeof($arrayRequestName); $i++) {
            if (isset($requestArray[$arrayRequestName[$i]])) {

                if ($requestArray[$arrayRequestName[$i]] == 0) {
                    $objects[$arrayField[$i]] = 0;
                } else {
                    $repository   = $em->getRepository($arrayClass[$i]);
                    $findFunction = 'findOneById';
                    $subObject    = $repository->$findFunction($requestArray[$arrayRequestName[$i]]);

                    if (!isset($subObject)) {
                        throw new HttpException(400, 'No object (' . $arrayField[$i] . ').');
                    }
                    $objects[$arrayField[$i]] = $subObject;
                }
            }
        }

        //устанавливаем значения
        foreach ($properties as $prop) {

            $setter = 'set' . ucfirst($prop->getName());

            $valueRequest = (isset($requestArray[$prop->getName()])) ? $requestArray[$prop->getName()] : null;//$request->get($prop->getName());
            $valueObject  = (isset($objects[$prop->getName()])) ? $objects[$prop->getName()] : null;

            if (isset($valueObject) && $reflect->hasMethod($setter)) {
                if ($valueObject === 0) {
                    $object->$setter(null);
                } else {
                    $object->$setter($valueObject);
                }
            } else if (isset($valueRequest) && $reflect->hasMethod($setter) && !in_array($prop->getName(), $arrayField)
            ) {
                $object->$setter($valueRequest);
            }
        }

        $this->throwErrorIfNotValid($object);

        $em->persist($object);
        try {
            $em->flush();
            if (!is_null($afterFunction)) {
                call_user_func($afterFunction);
            }
        } catch
        (\Exception $e) {
            throw new HttpException(400, 'Error with Database ' . $e->getMessage());
        }

        return $object;
    }
}