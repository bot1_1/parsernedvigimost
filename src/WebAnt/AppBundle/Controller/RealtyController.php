<?php
//
//namespace WebAnt\AppBundle\Controller;
//
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
//
///**
// * Hello controller.
// *
// * @Route("/admin/hello")
// */
//class HelloController extends Controller
//{
//    /**
//     * @Route("/{name}")
//     * @Template()
//     */
//    public function indexAction($name)
//    {
//        $admin_pool = $this->get('sonata.admin.pool');
//
//        return array(
//            'admin_pool' => $admin_pool,
//            'name' => $name
//        );
//    }
//
//}