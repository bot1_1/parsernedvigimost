<?php


namespace WebAnt\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class StatsController extends Controller
{
    /**
     * @Route("/admin/stats/", name="stats")
     */
    public function indexAction()
    {
        $admin_pool = $this->get('sonata.admin.pool');

        $user = $this->getUser();
        if($user->getRole0()!='ROLE_SUPER_ADMIN'){
            return $this->render('WebAntAppBundle::stats.html.twig',
                array('admin_pool' => $admin_pool,
                    'cols'  => array(),
                    'users' => array(),
                ));
        }

        $cols = array(
            array('label' => 'Менеджер'               , 'param' => 'fio'          ),
            array('label' => 'Последний заход'        , 'param' => 'last_login'   ),
            array('label' => 'Заходов'                , 'param' => 'logins'       ),
            array('label' => 'Новые клиенты'          , 'param' => 'new_users'    ),
            array('label' => 'Новые рекламные клиенты', 'param' => 'new_adv_users'),
            array('label' => 'Новые объекты'          , 'param' => 'new_realtis'  ),
            array('label' => 'Редактирование объектов', 'param' => 'edit_realtis' ),
        );


        $em = $this->getDoctrine()->getManager();

        $users_query = $em->createQuery(
            'SELECT u
                FROM WebAntAppBundle:User u'
        );
        $users = $users_query->getResult();

        foreach($users as $user){
            if(in_array($user->getRole0(),array('ROLE_REALTY_ADMIN','ROLE_SUPER_ADMIN'))){
                $rows[] = array(
                    'user_object'   => $user,
                    'user_id'       => $user->getId(),
                    'fio'           => $user->getFIO(),
                    'last_login'    => 0,
                    'logins'        => 0,
                    'new_users'     => 0,
                    'new_adv_users' => 0,
                    'new_realtis'   => 0,
                    'edit_realtis'  => 0,
                );
            }
        }

        $events_query = $em->createQuery(
            'SELECT e
                FROM WebAntAppBundle:History e'
        );
        $events = $events_query->getResult();

        foreach($events as $event){
            $user = $this->getById($rows, $event->getUserId()->getId());

//            $rows[$user]["logins"] .= ' '.$event->getType();
            switch ($event->getType()) {
                case 'create_user':
                    $rows[$user]["new_users"]++;
                    break;
                case 'create_adv_user':
                    $rows[$user]["new_adv_users"]++;
                    break;
                case 'create_realty':
                    $rows[$user]["new_realtis"]++;
                    break;
                case 'edit_realty':
                    $rows[$user]["edit_realtis"]++;
                    break;
                case 'login':
                    $rows[$user]["logins"]++;
                    $rows[$user]["last_login"] = $this->date2string($event->getDate());
                    break;
            }
        }




        return $this->render('WebAntAppBundle::stats.html.twig',
            array('admin_pool' => $admin_pool,
                'cols'  => $cols,
                'users' => $rows,
            ));
    }


    private function date2string($date){
        $d = $date->format('d');
        $m = array('' ,
            'января'  ,
            'февраля' ,
            'марта'   ,
            'апреля'  ,
            'мая'     ,
            'июня'    ,
            'июля'    ,
            'августа' ,
            'сентября',
            'октября',
            'ноября',
            'декабря',
        )[$date->format('m')-0];
        $y = $date->format('Y H:m:s');
        return "$d $m $y";
    }



    private function getById($array, $id){
        for($i = 0; $i < count($array); $i++){
            if($array[$i]["user_id"] == $id){
                return $i;
            }
        }
    }


}