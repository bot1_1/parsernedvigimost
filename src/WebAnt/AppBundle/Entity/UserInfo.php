<?php

namespace WebAnt\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;


/**
 * @ORM\Entity
// * @ORM\Table(name="user_info")
 */

class UserInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="WebAnt\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user_id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=100, nullable=true)
     */
    private $middle_name;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_price", type="integer", length=100, nullable=true)
     */
    private $min_price;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_price", type="integer", length=100, nullable=true)
     */
    private $max_price;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contract_id", type="string", length=25, nullable=true)
     */
    private $contract_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="contract_price", type="integer", length=25, nullable=true)
     */
    private $contract_price;

    /**
     * @var boolean
     *
     * @ORM\Column(name="rehash", type="boolean", nullable=true)
     */
    private $rehash;

    /**
     * @var integer
     *
     * @ORM\Column(name="rehash_price", type="integer", length=25, nullable=true)
     */
    private $rehash_price;

    /**
     * @var array
     *
     * @ORM\Column(name="realty_types", type="array", length=25, nullable=true)
     */
    private $realty_types;


    /**
     * @var array
     *
     * @ORM\Column(name="districts", type="array", length=300, nullable=true)
     */
    private $districts;

    /**
     * @var string
     *
     * @ORM\Column(name="creator", type="string", length=100, nullable=true)
     */
    private $creator;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * @param string $middle_name
     */
    public function setMiddleName($middle_name)
    {
        $this->middle_name = $middle_name;
    }

    /**
     * @return int
     */
    public function getMinPrice()
    {
        return $this->min_price;
    }

    /**
     * @param int $min_price
     */
    public function setMinPrice($min_price)
    {
        $this->min_price = $min_price;
    }

    /**
     * @return int
     */
    public function getMaxPrice()
    {
        return $this->max_price;
    }

    /**
     * @param int $max_price
     */
    public function setMaxPrice($max_price)
    {
        $this->max_price = $max_price;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getContractId()
    {
        return $this->contract_id;
    }

    /**
     * @param string $contract_id
     */
    public function setContractId($contract_id)
    {
        $this->contract_id = $contract_id;
    }

    /**
     * @return int
     */
    public function getContractPrice()
    {
        return $this->contract_price;
    }

    /**
     * @param int $contract_price
     */
    public function setContractPrice($contract_price)
    {
        $this->contract_price = $contract_price;
    }

    /**
     * @return boolean
     */
    public function isRehash()
    {
        return $this->rehash;
    }

    /**
     * @param boolean $rehash
     */
    public function setRehash($rehash)
    {
        $this->rehash = $rehash;
    }

    /**
     * @return int
     */
    public function getRehashPrice()
    {
        return $this->rehash_price;
    }

    /**
     * @param int $rehash_price
     */
    public function setRehashPrice($rehash_price)
    {
        $this->rehash_price = $rehash_price;
    }

    /**
     * @return array
     */
    public function getRealtyTypes()
    {
        return $this->realty_types;
    }

    /**
     * @param array $realty_types
     */
    public function setRealtyTypes($realty_types)
    {
        $this->realty_types = $realty_types;
    }

    /**
     * @return array
     */
    public function getDistricts()
    {
        return $this->districts;
    }

    /**
     * @param array $districts
     */
    public function setDistricts($districts)
    {
        $this->districts = $districts;
    }

    /**
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }


}
