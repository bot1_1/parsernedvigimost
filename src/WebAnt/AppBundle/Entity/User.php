<?php

namespace WebAnt\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManager;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use JMS\Serializer\Annotation\Exclude;
use WebAnt\AppBundle\Entity\UserInfo as UserInfo;


/**
 * @ORM\Entity
 * @ORM\Table(name="user",indexes={@ORM\Index(name="username_idx", columns={"username"})})
 */

class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\OneToOne(targetEntity="WebAnt\AppBundle\Entity\UserInfo",cascade={"persist"})
     * @ORM\JoinColumn(name="user_info", referencedColumnName="id")
     **/
    private $user_info;

    protected $enabled = true;

    public function __construct()
    {
        parent::__construct();

        $this->setRole0('ROLE_REALTY_USER');
        $this->enabled = true;
//        $this->user_info = new UserInfo();
    }

    public function getGenderRus() {

        $labels = array(
            'u' => 'Не указан',
            'm' => 'Мужской',
            'f' => 'Женский',
        );
        if(!isset($this->gender)){
            return "";
        }
        return $labels[$this->gender];
    }


    public function getRoleSimple() {
        $labels = array(
            'ROLE_ADV_USER'     => 'Рекламный пользователь',
            'ROLE_REALTY_USER'  => 'Обычный пользователь',
            'ROLE_REALTY_ADMIN' => 'Менеджер',
            'ROLE_SUPER_ADMIN'  => 'Администратор',
        );
        if(!count($this->roles)){
            return "Нестандартная роль";
        }

        return $labels[$this->roles[0]];
    }


    public function getRole0() {
//        return $this->getRoleSimple();
        if(!count($this->roles)){
            return "ROLE_REALTY_USER";
        }
        return $this->roles[0];
    }

    public function setRole0($role) {
        $this->setRoles(array(
            $role
        ));
        if($role=='ROLE_ADV_USER'){
            $this->setEnabled(false);
        } else {
            $this->setEnabled(true);
        }
    }

    /**
     * @return mixed
     */
    public function getUserInfo()
    {
//        if(!$this->getUserInfo()){
//            $this->user_info = new UserInfo();
//        }
//        $this->user_info = new UserInfo();
        $user_info = $this->user_info;
        if(is_null($user_info)){
            $this->user_info = new UserInfo();
        }
        return $this->user_info;
    }

    /**
     * @param mixed $user_info
     */
    public function setUserInfo($user_info)
    {
        $this->user_info = $user_info;
    }



    private function getAbstractUserInfoParam($getFuncName){
        $user_info = $this->user_info;
        if(is_null($user_info)){
            return null;
        } else {
            return $this->user_info->{$getFuncName}();
        }
    }

    private function setAbstractUserInfoParam($setFuncName, $value){
        $user_info = $this->user_info;
        if(is_null($user_info)){
            $this->user_info = new UserInfo();
        }
        $this->user_info->{$setFuncName}($value);
    }

    /**
     * @return string
     */
    public function getFIO()
    {
        return $this->lastname." ".$this->firstname." ".$this->getAbstractUserInfoParam('getMiddleName');
    }


    /**
     * @return string
     */
    public function getRealtyString()
    {
        $types = $this->getAbstractUserInfoParam('getRealtyTypes');

        if(!is_array($types)){
            return "";
        }

        $allTypes=array(
            "Комнату",
            "1-комнатную квартиру",
            "2-комнатную квартиру",
            "3 или более-комнатную квартиру",
            "",
            "Дом"
        );
        $string = "";

        foreach (array(0,1,2,3,5) as $type){
            if(in_array($type,$types)){
                $string .= $allTypes[$type] . ", ";
            }
        }
        $string = substr($string, 0, -2);

        return $string;
    }



    /**
     * @param array $realty_types
     */
    public function setRealtyTypes($realty_types)
    {
        $this->setAbstractUserInfoParam('setRealtyTypes', $realty_types);
    }






}