<?php

namespace WebAnt\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Realty
 *
 * @ORM\Table(name="realty")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Realty
{
    public function __construct()
    {
        $this->date = new \DateTime();
        $this->date_update = new \DateTime();
        $this->status = 0;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModifiedDatetime() {
        // update the modified time
        $this->setDateUpdate(new \DateTime());
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $old_id;


    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime", nullable=true)
     */
    private $date_update;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="price_our", type="integer", nullable=true)
     */
    private $price_our;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float", precision=10, scale=0, nullable=true)
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="distr", type="string", length=100, nullable=true)
     */
    private $distr;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=100, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="addr", type="string", length=100, nullable=true)
     */
    private $addr;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneshow", type="string", length=42, nullable=true)
     */
    private $phoneshow;

    /**
     * @var string
     *
     * @ORM\Column(name="stext", type="string", length=1000, nullable=true)
     */
    private $stext;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=1000, nullable=true)
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    /**
     * @var string
     *
     * @ORM\Column(name="who_edit", type="string", nullable=true)
     */
    private $who;



    /**
     * @return int
     */
    public function getOldId()
    {
        return $this->old_id;
    }

    /**
     * @param int $old_id
     */
    public function setOldId($old_id)
    {
        $this->old_id = $old_id;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTypeLabel()
    {
        $types=array(
            "Комната",
            "1-комнатная квартира",
            "2-комнатная квартира",
            "3 или более-комнатная квартира",
            "",
            "Дом"
        );
        if(!isset($this->type)){
            return "";
        }

        return $types[$this->type];
    }

    /**
     * @param int $type
     */
    public function setTypeLabel($type)
    {
        $this->type = $type;
    }
    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getDateRus()
    {
        return $this->date2Rus($this->date);
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    private function date2Rus($date){
        $d = $date->format('d');
        $m = array('' ,
            'января'  ,
            'февраля' ,
            'марта'   ,
            'апреля'  ,
            'мая'     ,
            'июня'    ,
            'июля'    ,
            'августа' ,
            'сентября',
            'октября',
            'ноября',
            'декабря',
        )[$date->format('m')-0];
        $y = $date->format('Y H:m:s');
        return "$d $m $y";
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @return string
     */
    public function getDateUpdateRus()
    {
        return $this->date2Rus($this->date_update);
    }

    /**
     * @param int $date_update
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getPriceOur()
    {
        return $this->$price_our;
    }

    /**
     * @param int $price_our
     */
    public function setPriceOur($price_our)
    {
        $this->$price_our = $price_our;
    }

    /**
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param float $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return string
     */
    public function getDistr()
    {
        return $this->distr;
    }

    /**
     * @param string $distr
     */
    public function setDistr($distr)
    {
        $this->distr = $distr;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getAddr()
    {
        return $this->addr;
    }

    /**
     * @param string $addr
     */
    public function setAddr($addr)
    {
        $this->addr = $addr;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhoneShow()
    {
        return $this->phoneshow;
    }

    /**
     * @param string $phoneshow
     */
    public function setPhoneShow($phoneshow)
    {
        $this->phoneshow = $phoneshow;
    }

    /**
     * @return string
     */
    public function getStext()
    {
        return $this->stext;
    }

    /**
     * @param string $stext
     */
    public function setStext($stext)
    {
        $this->stext = $stext;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusString()
    {
        if(!isset($this->status)){
            return 'Не проверен';
        }

        return array(
            'Не проверен',  //0
            'Недоступен',   //1
            'Не отвечает',  //2
            'Сдан',         //3
            'В работе',     //4
            'Архивный',     //5
        )[$this->status];
    }




    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getWho()
    {
        return $this->who;
    }



    /**
     * @param string $who
     */
    public function setWho($who)
    {
        $this->who = $who;
    }








    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
