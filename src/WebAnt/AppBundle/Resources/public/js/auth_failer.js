/**
 * Created by kwant on 14.05.15.
 */


$(function(){
    var getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    var auth = getParameterByName('auth');
    //console.log($(".last i::after").text().after().text());


    if(auth == 'fail'){
        $(".last i").trigger("click");
        $(".g-popup__error").show();
    }
    if(auth == 'auth'){
        $(".last i").trigger("click");
    }

});
