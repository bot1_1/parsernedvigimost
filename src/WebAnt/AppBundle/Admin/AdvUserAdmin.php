<?php

namespace WebAnt\AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use Symfony\Component\Validator\Constraints\DateTime;
use WebAnt\AppBundle\Entity\User;
use WebAnt\AppBundle\Entity\History;
use WebAnt\AppBundle\Data\Data;
use WebAnt\AppBundle\Service\SaveHistory;


use Sonata\CoreBundle\Form\Type\EqualType;
use WebAnt\AppBundle\Service\SmsService;

use WebAnt\AppBundle\Service\UserRealtisService;



class AdvUserAdmin extends Admin
{
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

/*
        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page'       => 1,
                '_sort_order' => 'DESC',      // sort direction
                '_sort_by'    => 'date' // field name
            );
        }
*/
    }


    protected $baseRouteName = 'webant_appbundle_advuseradmin';
    protected $baseRoutePattern = 'adv_user';
    protected $translationDomain = 'WebAntAppBundle'; // default is 'messages'

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->eq($query->getRootAliases()[0] . '.roles', ':adv_user')
        );
        $query->setParameter('adv_user', 'a:1:{i:0;s:13:"ROLE_ADV_USER";}');
        return $query;
    }

    public function preUpdate($user)
    {
        $a= $this->getSubject()->getRole0();
        if($a!='ROLE_ADV_USER'){
            return false;
        }
        $user->setUserName(microtime(true).'');
    }


    public function postRemove($user)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $hist->saveEntry($em, $realtor, 'delete_adv_user', ($user->getId()));
    }

    public function postUpdate($user)
    {
        $a= $this->getSubject()->getRole0();
        if($a!='ROLE_ADV_USER'){
            return false;
        }
        $user->setUserName(microtime(true).'');

        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $hist->saveEntry($em, $realtor, 'edit_adv_user', $user->getId());
    }

    public function postPersist($user){


        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $hist->saveEntry($em, $realtor, 'create_adv_user', $user->getId());
//        $hist->saveEntry($em, $realtor, 'create_adv_user', '');

        $tlf = $user->getUserInfo()->getPhone();
        $tlf = preg_replace("/[^0-9]/","",$tlf);
        if(strlen($tlf)){
            if($tlf[0]=='8'){
                $tlf[0]='7';
            }
        } else {
            return false;
        }

        $DM = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $check = new UserRealtisService();
        $res = $check -> checkUser($DM,$user);
        $count = $res['count'];



        $name  = $realtor->getFirstName();
        $phone = $realtor->getUserInfo()->getPhone();
        $address = $realtor->getUserInfo()->getAddress();

        if(!isset($address)){
            $address = 'Варфоломеева 259, 6 этаж, оф.601.';
        }

        $text1 = "Вас ожидают в агентстве «Дон-эстейт». Варфоломеева 259, 6 этаж, оф.601";
//        $text2 = "Вам назначена встреча в агентстве недвижимости «Дон-эстейт». Варфоломеева 259, 6 этаж, оф.601. Ваш менеджер $name. Тел. $phone";
        $text2 = "Вам назначена встреча в агентстве недвижимости «Дон-эстейт». $address Ваш менеджер $name. Тел. $phone";

//        $file = "/mnt/tmpfs/arenda.log";
//        file_put_contents($file,"\n$text1\n$text2\n",FILE_APPEND);
//        file_put_contents($file,"sendsms $tlf \"вам подходит $count объектов\"\n",FILE_APPEND);


        $sms = new SmsService();
        $sms->sendSms($tlf,$text2);

/*
"Вас ожидают в агентстве «Дон-эстейт». Варфоломеева 259, 6 этаж, оф.601"
"Вам назначена встреча в агентстве недвижимости «Дон-эстейт». Варфоломеева 259, 6 этаж, оф.601. Ваш менеджер Анастасия. Тел. 89286286353"
 *
 * */
    }


    public function prePersist($user)
    {
        $user->setUserName(microtime(true).'');
        $user->setEmail(microtime(true).'@asdf.ru');

        $user->setRole0('ROLE_ADV_USER');
        $user->setPassword('0');
//        $user->setUserName('persist');
        $realtor = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $name = $realtor->getFIO();
        $user->getUserInfo()->setCreator($name);
    }



    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {

        $showMapper
            ->with('Профиль')
                ->add('fio','text',array(
                    'label' => 'ФИО'
                ))
                ->add('gender_rus',null,array(
                    'label' => 'Пол'
                ))
                ->add('user_info.address','text',array(
                    'label' => 'Адрес'
                ))
                ->add('user_info.phone','text',array(
                    'label' => 'Телефон'
                ))
            ->end()
            ->with('Дополнительная информация')
                ->add('realtyString','text',
                    array(
                        'label' => 'Ищет'
                    )
                )
                ->add('user_info.minPrice','number',array(
                    'label' => 'Цена от:'
                ))
                ->add('user_info.maxPrice','number',array(
                    'label' => 'Цена до:'
                ))
                ->add('user_info.contractId','text',array(
                    'label' => '№ договора'
                ))
                ->add('user_info.contractPrice','number',array(
                    'label' => 'Цена договора'
                ))
                ->add('user_info.creator','text',
                    array(
                        'label' => 'Создал'
                    )
                )
            ->end()
//            ->add('')
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->getSubject();

        $a= $this->getSubject()->getRole0();
        $b= $this->getSubject()->getId();
        if($a!='ROLE_ADV_USER'&&$b){
            $formMapper
                ->with("Вы полезли туда, куда вам не следует.")
                ->end();
            return false;
        }

        $d = new Data();
        $disrticts = $d->getDistrictsArray();

        $realtyTypes = array(
            '0' => 'Комнату',
            '1' => '1-комнатную квартиру',
            '2' => '2-х комнатную квартиру',
            '3' => '3-х и более комнатную квартиру',
            '5' => 'Дом',
        );

        $formMapper
            ->with('Профиль')
                ->add('lastname','text',array(
                    'label' => 'Фамилия',
                    'required' => false,
                ))
                ->add('firstname','text',array(
                    'label' => 'Имя',
                    'required' => false,
                ))
                ->add('user_info.middlename','text',array(
                    'label' => 'Отчество',
                    'required' => false,
                ))
                ->add('gender','choice',
                    array(
                        'choices' => array(
                            'u' => 'Не указан',
                            'm' => 'Мужской',
                            'f' => 'Женский',
                        ),
                        'expanded' => false,
                        'multiple' => false
                    )
                )
            ->end()
            ->with('Дополнительная информация')
                ->add('user_info.address','text',array(
                    'label' => 'Адрес',
                    'required' => false,
                ))
                ->add('user_info.phone','text',array(
                    'label' => 'Телефон',
                    'required' => false,
                ))
                ->add('user_info.minPrice','number',array(
                    'label' => 'Цена от:',
                    'required' => false,
                ))
                ->add('user_info.maxPrice','number',array(
                    'label' => 'Цена до:',
                    'required' => false,
                ))
                ->add('user_info.realtyTypes','choice',
                    array(
                        'choices' => $realtyTypes,
                        'expanded' => true,
                        'multiple' => true,
                        'label'    => 'Ищет'
                    )
                )
                ->add('user_info.districts','choice',
                    array(
                        'choices' => $disrticts,
                        'expanded' => true,
                        'multiple' => true,
                        'label'    => 'В районах'
                    )
                )
                ->add('user_info.contractId','text',array(
                    'label' => '№ договора',
                    'required' => false,
                ))
                ->add('user_info.contractPrice','number',array(
                    'label' => 'Цена договора',
                    'required' => false,
                ))
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('fio','text',array(
                'label' => 'ФИО'
            ))
            ->add('user_info.phone','text',array(
                'label' => 'Телефон'
            ))
            ->add('gender_rus',null,array(
                'label' => 'Пол'
            ))
            ->add('user_info.address',null,array(
                'label' => 'Адрес'
            ))
            ->add('realtyString','text',
                array(
                    'label' => 'Ищет'
                )
            )
            ->add('user_info.creator','text',
                array(
                    'label' => 'Создал'
                )
            )
            ->add('createdAt', 'datetime', array(
                'label' => 'время добавления'
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label'=>'Действия'
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
//    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
//    {
//        $datagridMapper
////            ->add('username')
//            ->add('user_info.address',null,array(
//                'label' => 'Адрес'
//            ))
//        ;
//    }



}