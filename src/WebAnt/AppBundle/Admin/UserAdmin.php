<?php

namespace WebAnt\AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;

use Symfony\Component\Validator\Constraints\DateTime;
use WebAnt\AppBundle\Entity\User;
use WebAnt\AppBundle\Data\Data;
use WebAnt\AppBundle\Service\SaveHistory;



class UserAdmin extends Admin
{
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
/*
        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page'       => 1,
                '_sort_order' => 'DESC',      // sort direction
                '_sort_by'    => 'date' // field name
            );
        }
*/
    }

    protected $baseRouteName = 'webant_appbundle_useradmin';
    protected $baseRoutePattern = 'user';
    protected $translationDomain = 'WebAntAppBundle'; // default is 'messages'

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $role = $user->getRole0();

        $query->andWhere(
            $query->expr()->in($query->getRootAliases()[0] . '.roles', ':users')
        );
        $query->setParameter('users', array(
            'a:1:{i:0;s:16:"ROLE_REALTY_USER";}'
        ));
        return $query;
    }

    public function postUpdate($user)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $hist->saveEntry($em, $realtor, 'edit_user', $user->getId());
    }

    public function postPersist($user)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $hist->saveEntry($em, $realtor, 'create_user', $user->getId());
    }
    public function postRemove($user)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $hist->saveEntry($em, $realtor, 'delete_user', ($user->getId()));
    }
    public function prePersist($user)
    {
        $realtor = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $name = $realtor->getFIO();
        $user->getUserInfo()->setCreator($name);
    }



    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $role = $user->getRole0();
        $a= $this->getSubject()->getRole0();
        $b= $this->getSubject()->getId();
        if(in_array($a,array('ROLE_SUPER_ADMIN','ROLE_REALTY_ADMIN'))&&$b && $role!= 'ROLE_SUPER_ADMIN'){
            $showMapper
                ->with("Вы полезли туда, куда вам не следует.")
                ->end();
            return false;
        }

        $showMapper
            ->with('Основная информация')
                ->add('username')
                ->add('email')
            ->end()
            ->with('Профиль')
                ->add('fio','text',array(
                    'label' => 'ФИО'
                ))
                ->add('gender_rus',null,array(
                    'label' => 'Пол'
                ))
                ->add('user_info.address','text',array(
                    'label' => 'Адрес'
                ))
                ->add('phone','text',array(
                    'label' => 'Телефон'
                ))
            ->end()
            ->with('Дополнительная информация')
                ->add('realtyString','text',
                    array(
                        'label' => 'Ищет'
                    )
                )
                ->add('user_info.minPrice','number',array(
                    'label' => 'Цена от:'
                ))
                ->add('user_info.maxPrice','number',array(
                    'label' => 'Цена до:'
                ))
                ->add('user_info.contractId','text',array(
                    'label' => '№ договора'
                ))
                ->add('user_info.contractPrice','number',array(
                    'label' => 'Цена договора'
                ))
                ->add('user_info.creator','text',
                    array(
                        'label' => 'Создал'
                    )
            )
            ->end()
//            ->add('')
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {


        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $role = $user->getRole0();
        $a= $this->getSubject()->getRole0();
        $b= $this->getSubject()->getId();
        if(in_array($a,array('ROLE_SUPER_ADMIN','ROLE_REALTY_ADMIN'))&&$b && $role!= 'ROLE_SUPER_ADMIN'){
            $formMapper
                ->with("Вы полезли туда, куда вам не следует.")
                ->end();
            return false;
        }


        $d = new Data();

        $disrticts = $d->getDistrictsArray();

//        new Data()->getDis
        $realtyTypes = array(
            '0' => 'Комнату',
            '1' => '1-комнатную квартиру',
            '2' => '2-х комнатную квартиру',
            '3' => '3-х и более комнатную квартиру',
            '5' => 'Дом',
        );


        $formMapper
            ->with('general')
                ->add('username')
                ->add('email')
                ->add('plainPassword', 'text', array(
                    'required' => (!$this->getSubject() || is_null($this->getSubject()->getId()))
                ))
            ->end()
            ->with('Профиль')
                ->add('lastname')
                ->add('firstname')
                ->add('user_info.middlename','text',array(
                    'label' => 'Отчество',
                    'required' => false,
                ))
                ->add('gender','choice',
                    array(
                        'choices' => array(
                            'u' => 'Не указан',
                            'm' => 'Мужской',
                            'f' => 'Женский',
                        ),
                        'expanded' => false,
                        'multiple' => false
                    )
                )
            ->end()
            ->with('Дополнительная информация')
                ->add('user_info.address','text',array(
                    'label' => 'Адрес',
                    'required' => false,
                ))
                ->add('user_info.phone','text',array(
                    'label' => 'Телефон',
                    'required' => false,
                ))
                ->add('user_info.minPrice','number',array(
                    'label' => 'Цена от:',
                    'required' => false,
                ))
                ->add('user_info.maxPrice','number',array(
                    'label' => 'Цена до:',
                    'required' => false,
                ))
                ->add('user_info.realtyTypes','choice',
                    array(
                        'choices' => $realtyTypes,
                        'expanded' => true,
                        'multiple' => true,
                        'label'    => 'Ищет'
                    )
                )
                ->add('user_info.districts','choice',
                    array(
                        'choices' => $disrticts,
                        'expanded' => true,
                        'multiple' => true,
                        'label'    => 'В районах'
                    )
                )
                ->add('user_info.contractId','text',array(
                    'label' => '№ договора',
                    'required' => false,
                ))
                ->add('user_info.contractPrice','number',array(
                    'label' => 'Цена договора',
                    'required' => false,
                ))
            ->end()
            ;


//        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
//        $role = $user->getRole0();
//        if($role == 'ROLE_SUPER_ADMIN'){
//            $roles_choices = array(
//                'ROLE_ADV_USER'     => 'Рекламный пользователь',
//                'ROLE_REALTY_USER'  => 'Обычный пользователь',
//                'ROLE_REALTY_ADMIN' => 'Администратор',
//                'ROLE_SUPER_ADMIN'  => 'Главный администратор',
//            );
//        } else {
//            $roles_choices = array(
//                'ROLE_ADV_USER'     => 'Рекламный пользователь',
//                'ROLE_REALTY_USER'  => 'Обычный пользователь',
//            );
//        }


        $formMapper
            ->with('Management')
//            ->add('role0','choice',
//                array(
//                    'choices' => $roles_choices,
//                    'expanded' => false,
//                    'multiple' => false,
//                    'label'    => 'Роль'
//                )
//            )
//            ->add('roles','choice', array(
//                    'expanded' => true,
//                    'multiple' => true)
//            )
            ->add('locked', null, array('required' => false))
            ->add('expired', null, array('required' => false))
            ->add('credentialsExpired', null, array('required' => false))
            ->end()
        ;



/*
        $formMapper
            ->with('Management')
                ->add('role0','choice',
                    array(
                        'choices' => array(
                            'ROLE_ADV_USER'     => 'Рекламный пользователь',
                            'ROLE_REALTY_USER'  => 'Обычный пользователь',
                            'ROLE_REALTY_ADMIN' => 'Администратор',
                            'ROLE_SUPER_ADMIN'  => 'Главный администратор',
                        ),
                        'expanded' => false,
                        'multiple' => false,
                        'label'    => 'Роль'
                    )
                )
//                ->add('role0','text')
                ->add('roles','choice', array(
                        'expanded' => true,
                        'multiple' => true)
                )
                ->add('locked', null, array('required' => false))
                ->add('expired', null, array('required' => false))
                ->add('credentialsExpired', null, array('required' => false))
            ->end()
        ;
*/


//
//
//        $subject = $this->getSubject();
//        if ($subject->getRole0() == 'ROLE_REALTY_USER') {
//            $formMapper
//            ;
//        }
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('email')
            ->add('fio','text',array(
                'label' => 'ФИО'
            ))
            ->add('user_info.phone','text',array(
                'label' => 'Телефон'
            ))
            ->add('gender_rus',null,array(
                'label' => 'Пол'
            ))
//            ->add('roleSimple',null,array(
//                'label' => 'Роль'
//            ))
            ->add('user_info.address',null,array(
                'label' => 'Адрес'
            ))
            ->add('realtyString','text',
                array(
                    'label' => 'Ищет'
                )
            )
            ->add('user_info.creator','text',
                array(
                    'label' => 'Создал'
                )
            )
            ->add('createdAt', 'datetime', array(
                'label' => 'время добавления'
            ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
                'label'=>'Действия'
            ))

//            ->add('roles')
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
//            ->add('username')
//            ->add('user_info.address',null,array(
//                'label' => 'Адрес'
//            ))
        ;
    }



}