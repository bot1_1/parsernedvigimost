<?php

namespace WebAnt\AppBundle\Admin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use WebAnt\AppBundle\Service\SaveHistory;

use Knp\Menu\ItemInterface as MenuItemInterface;

use WebAnt\AppBundle\Entity\Realty;

class ArchiveRealtyAdmin extends Admin
{
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page'       => 1,
                '_sort_order' => 'DESC',      // sort direction
                '_sort_by'    => 'date' // field name
            );
        }
    }

    protected $baseRouteName = 'webant_appbundle_arcrealtyadmin';
    protected $baseRoutePattern = 'arc_realty';
    protected $translationDomain = 'WebAntAppBundle'; // default is 'messages'

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->eq($query->getRootAliases()[0] . '.status', ':status')
        );
        $query->setParameter('status', '5');
        return $query;
    }


    public function postUpdate($realty)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $params = $realty->getId()."";
        $hist->saveEntry($em, $realtor, 'edit_realty', $params);
    }


    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
//                ->add('date', 'datetime', array(
//                    'format' => 'j F Y',
////                'format' => 'a-b-c-d-e-f-g-h-i-j-k-l-m-n-o-p-q-r-s-t-u-v-w-x-y-z',
//                    'locale' => 'ru',
//                    'timezone' => 'Russia/Moscow'
//                ))
                ->add('date', 'datetime', array(
                    'label' => 'Дата создания'
                ))
                ->add('date_update', 'datetime', array(
                    'label' => 'Последнее изменение'
                ))
    //            ->add('dateUpdate', 'datetime', array(
    //                'format' => '',
    //                'label'  => '_',
    //            ))
                ->add('price')
                ->add('area')
                ->add('distr')
                ->add('street')
                ->add('addr')
                ->add('phone')
//                ->add('id', 'text', array('label' => 'Телефон(картинка)', 'template' => 'WebAntAppBundle::phone_img.html.twig'))
                ->add('who', 'text', array(
                        'label' => 'Кто редактировал'
                    )
                )
                ->add('stext')
                ->add('text')
//                ->add('who', 'text', array(
//                        'label' => 'Кто редактировал'
//                    )
//                )
            ->end()
        ;


        $old_id = $this->getSubject()->getOldId();

        if(isset($old_id)){
            $showMapper
                ->with('General')
                ->add('old_id', 'text', array('label' => 'Ссылка на оригинальный объект', 'template' => 'WebAntAppBundle::original_link.html.twig'))
                ->end();
        }

//        $this->getObjectIdentifier();

        $this->data='';
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
//                ->add('date','datetime')
                ->add('typeLabel','choice',
                    array(
                        'choices' => array(
                            '0' => 'Комната',
                            '1' => '1-комнатная квартира',
                            '2' => '2-х комнатная квартира',
                            '3' => '3-х и более комнатная квартира',
                            '5' => 'Дом',
                        ),
                        'expanded' => false,
                        'multiple' => false
                    )
                )

                ->add('status','choice',
                    array(
                        'choices' => array(
                            'Не проверен',
                            'Недоступен',
                            'Не отвечает',
                            'Сдан',
                            'В работе',
                        ),
                        'expanded' => false,
                        'multiple' => false
                    )
                )
                ->add('price')
                ->add('area')
                ->add('distr')
                ->add('street')
                ->add('addr')
                ->add('phone')
//                ->add('id')
//                ->add('id', 'text', array('label' => 'Телефон(картинка)', 'template' => 'WebAntAppBundle::phone_img.html.twig'))
//                ->add('id', 'text', array('label' => 'Телефон(картинка)', 'template' => 'WebAntAppBundle::phone_img.html.twig'))
                ->add('stext')
                ->add('text')
            ->end()
        ;


    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
//            ->add('date', 'datetime', array(
//                'format' => 'j F Y',
////                'format' => 'a-b-c-d-e-f-g-h-i-j-k-l-m-n-o-p-q-r-s-t-u-v-w-x-y-z',
//                'locale' => 'ru',
//                'timezone' => 'Russia/Moscow'
//            ))
            ->add('date', null, array(
                'label' => 'Дата создания'
            ))
            ->add('date_update', null, array(
                'label' => 'Последнее изменение',
            ))
            ->add('typeLabel')
            ->add('price')
            ->add('area')
            ->add('distr')
//            ->add('street')
            ->add('addr')
            ->add('phone')
//            ->add('stext')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('price')
            ->add('area','doctrine_orm_number',array(
                'type' => 'range'
            ))
            ->add('distr')
            ->add('addr')
            ->add('text')
            ->add('type', 'doctrine_orm_choice', array(
                'label' => 'Тип'),
                'choice',
                array(
                    'choices' => array(
                        '0' => 'Комнаты',
                        '1' => '1-комнатные квартиры',
                        '2' => '2-х комнатные квартиры',
                        '3' => '3-х и более комнатные квартиры',
                        '5' => 'Дома',
                    ),
                    'expanded' => true,
                    'multiple' => true
                )
            );
        ;
    }

    public function toString($object) {
        if (!is_object($object)) {
            return '';
        }
        if (method_exists($object, '__toString') && null !== $object->__toString()) {
            return (string) $object;
        }

        $cname = explode('\\', get_class($object));

        return "Архивная Недвижимость";
        //return end($cname);

    }



}