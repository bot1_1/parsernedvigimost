<?php

namespace WebAnt\AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;


use Knp\Menu\ItemInterface as MenuItemInterface;

use WebAnt\AppBundle\Entity\Realty;
use WebAnt\AppBundle\Data\Data;
use WebAnt\AppBundle\Service\SaveHistory;


class RealtyAdmin extends Admin
{
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_page'       => 1,
                '_sort_order' => 'DESC',      // sort direction
                '_sort_by'    => 'date_update' // field name
            );
        }
    }

    protected $translationDomain = 'WebAntAppBundle'; // default is 'messages'
//    protected $baseRoutePattern = 'arc_realty';


    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->neq($query->getRootAliases()[0] . '.status', ':status')
        );
        $query->setParameter('status', '5');

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $role = $user->getRole0();

        if(!in_array($role,array('ROLE_REALTY_ADMIN','ROLE_SUPER_ADMIN'))){
            $data = new Data();
            $uf = $user->getUserInfo();

            $distr = $uf->getDistricts();
            if(count($distr)){
                $query->andWhere(
                    $query->expr()->in($query->getRootAliases()[0] . '.distr', ':distr')
                );
                $query->setParameter('distr', $data->ids2strings($distr));
            }

            $types = $uf->getRealtyTypes();
            if(count($types)){
                $query->andWhere(
                    $query->expr()->in($query->getRootAliases()[0] . '.type', ':types')
                );
                $query->setParameter('types', $types);
            }
            $min_price = $uf->getMinPrice();
            if($min_price > 0){
                $query->andWhere(
                    $query->expr()->gte($query->getRootAliases()[0] . '.price', ':min_price')
                );
                $query->setParameter('min_price', $min_price);
            }
            $max_price = $uf->getMaxPrice();
            if($max_price > 0){
                $query->andWhere(
                    $query->expr()->lte($query->getRootAliases()[0] . '.price', ':max_price')
                );
                $query->setParameter('max_price', $max_price);
            }
        }

//        var_dump($this->getRequest());

//        if (!$this->hasRequest()) {
//            $query->addOrderBy($query->getRootAliases()[0] . '.date_update', 'DESC');
//        }


        return $query;
    }


    public function postUpdate($realty)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $params = $realty->getId().", ".$realty->getDistr();
        $hist->saveEntry($em, $realtor, 'edit_realty', $params);
    }

    public function postPersist($realty)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $params = $realty->getId().", ".$realty->getDistr();

        $hist->saveEntry($em, $realtor, 'create_realty', $realty->getId());
    }

    public function postRemove($realty)
    {
        $realtor= $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $hist = new SaveHistory();
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $hist->saveEntry($em, $realtor, 'delete_user', ($realty->getId()));
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
//                ->add('date', 'datetime', array(
//                    'format' => 'j F Y',
////                'format' => 'a-b-c-d-e-f-g-h-i-j-k-l-m-n-o-p-q-r-s-t-u-v-w-x-y-z',
//                    'locale' => 'ru',
//                    'timezone' => 'Russia/Moscow'
//                ))
                ->add('dateRus', null, array(
                    'label' => 'Дата создания'
                ))
                ->add('dateUpdateRus', null, array(
                    'label' => 'Последнее изменение'
                ))
                ->add('price')
                ->add('area')
                ->add('distr')
                ->add('street')
                ->add('addr')
                ->add('phone')
                ->add('statusString',null,array(
                    'label' => 'Статус'
                ))
//                ->add('id'    , 'text', array('label' => 'Телефон(картинка)', 'template' => 'WebAntAppBundle::phone_img.html.twig'))
                ->add('stext')
                ->add('text')
            ->end()
        ;
//        $this->getObjectIdentifier();

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $role = $user->getRole0();
        $old_id = $this->getSubject()->getOldId();

        if(in_array($role,array('ROLE_REALTY_ADMIN','ROLE_SUPER_ADMIN'))){
            $showMapper
                ->with('General')
                    ->add('who', 'text', array(
                            'label' => 'Кто редактировал'
                        )
                    )
                ->end();
        }


        if(in_array($role,array('ROLE_REALTY_ADMIN','ROLE_SUPER_ADMIN')) && isset($old_id)){
            $showMapper
                ->with('General')
                    ->add('old_id', 'text', array('label' => 'Ссылка на оригинальный объект', 'template' => 'WebAntAppBundle::original_link.html.twig'))
                ->end();
        }

//        'Ссылка на оригинальный объект'
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
//                ->add('date','datetime')
                ->add('typeLabel','choice',
                    array(
                        'choices' => array(
                            '0' => 'Комната',
                            '1' => '1-комнатная квартира',
                            '2' => '2-х комнатная квартира',
                            '3' => '3-х и более комнатная квартира',
                            '5' => 'Дом',
                        ),
                        'expanded' => false,
                        'multiple' => false
                    )
                )
                ->add('status','choice',
                    array(
                        'choices' => array(
                            'Не проверен',
                            'Недоступен',
                            'Не отвечает',
                            'Сдан',
                            'В работе',
                            'Архивный',
                        ),
                        'expanded' => false,
                        'multiple' => false,
                        'label'    => "Статус"
                    )
                )
                ->add('price')
                ->add('area')
                ->add('distr')
                ->add('street')
                ->add('addr')
//                ->add('phone')
//                ->add('id'    , 'text', array('label' => 'Телефон(картинка)', 'template' => 'WebAntAppBundle::phone_img.html.twig'))
//                ->add('phone', 'text', array('label' => 'Телефон', 'template' => 'WebAntAppBundle::phone_img2.html.twig'))

//                ->add('id')
//                ->add('id', 'text', array('label' => 'Телефон(картинка)', 'template' => 'WebAntAppBundle::phone_img.html.twig'))
//                ->add('id', 'text', array('label' => 'Телефон(картинка)', 'template' => 'WebAntAppBundle::phone_img.html.twig'))
                ->add('stext')
                ->add('text')
            ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
//            ->add('date', 'datetime', array(
//                'format' => 'j M Y',
////                'format' => 'a-b-c-d-e-f-g-h-i-j-k-l-m-n-o-p-q-r-s-t-u-v-w-x-y-z',
//                'locale' => 'ru-ru',
//                'timezone' => 'Russia/Moscow'
//            ))
            ->add('date', 'datetime', array(
                'label' => 'Дата создания',
            ))
            ->add('date_update', 'datetime', array(
                'label'    => 'Последнее изменение',
//                'sortable'=>true,
//                'sort_field_mapping'=> array('fieldName'=>'date_update'),
            ))
//            ->add('dateUpdate', 'datetime', array(
//                'format' => '',
//                'label'  => '_',
//            ))
            ->add('typeLabel')
            ->add('price')
            ->add('area')
            ->add('distr')
//            ->add('street')
            ->add('addr')
            ->add('phone')
            ->add('statusString',null,array(
                'label' => 'Статус'
            ))
//            ->add('stext')
        ;

        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $role = $user->getRole0();
        if(in_array($role,array('ROLE_REALTY_ADMIN','ROLE_SUPER_ADMIN'))){
            $listMapper
                ->add('who', 'text', array(
                        'label' => 'Кто редактировал'
                    )
                );
        }
        $listMapper
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;

    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('price')
            ->add('area','doctrine_orm_number',array(
                'type' => 'range'
            ))
            ->add('distr')
            ->add('addr')
            ->add('text')
            ->add('type', 'doctrine_orm_choice', array(
                'label' => 'Тип'),
                'choice',
                array(
                    'choices' => array(
                        '0' => 'Комнаты',
                        '1' => '1-комнатные квартиры',
                        '2' => '2-х комнатные квартиры',
                        '3' => '3-х и более комнатные квартиры',
                        '5' => 'Дома',
                    ),
                    'expanded' => true,
                    'multiple' => true
                )
            );
        ;
    }

    public function prePersist($realty)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $name = $user->getFIO();
        $realty->setWho($name);
    }


    public function preUpdate($realty)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();
        $name = $user->getFIO();
        $realty->setWho($name);
    }



    public function toString($object) {
//        if (!is_object($object)) {
//            return '';
//        }
//        if (method_exists($object, '__toString') && null !== $object->__toString()) {
//            return (string) $object;
//        }
//
//        $cname = explode('\\', get_class($object));

        return "Недвижимость";
        //return end($cname);

    }



}

/*
'Ворошиловский'
'Железнодорожный'
'Кировский'
'Ленинский'
'Октябрьский'
'Первомайский'
'Пролетарский'
'Советский'
'1-й п. Орджоникидзе'
'2-й п. Орджоникидзе'
'Александровка'
'Берберовка'
'Болгарстрой'
'Военвед'
'ЗЖМ'
'Змиевка'
'Каменка'
'Левенцовский'
'Ленгородок'
'Мирный'
'Мясникован'
'Нахичевань'
'Новое поселение'
'Олимпиадовка'
'Сельмаш'
'СЖМ'
'Стройгородок'
'Темерник'
'Фрунзе'
'Чкаловский'
'Пригород'
'г. Азов'
'г. Аксай'
'г. Батайск'
'Ростовская область'
'г. Волгодонск'
'г. Новошахтинск'
'г. Таганрог'
'г. Шахты'




 * */


